SoundLabDecoderPatch {
	// copyArgs
	var <sl, <decoderName, decSpecs, inbusnum, outbusnum, <group, <stereoActive, <loadCondition;
	var <server, <decodersynth, <compsynth, <attributes, <decType;
	var <satOrder, <subOrder, <psyFoclSize, <alpha, <foclRadius, <mainBeamDict, <mainDim;
	var <match, <mainPsychoKernels, <subPsychoKernels, kernelPath, <psyFoclKernelPath, <subBeamDict;
	var subDim, spkrDists, convBus, <convSynth, <irbuffers, bufSizes, irspectrums, <convSynthDef;
	var numBChannels, numSubChannels, synthdefName, sampleRate, initialBus, <delayCompBus, <formatSynth, <formatBus;

	*new { |soundlab, decoderName, decSpecs, inbusnum, outbusnum, group, stereoActive, loadCondition|
		^super.newCopyArgs(soundlab, decoderName, decSpecs, inbusnum, outbusnum, group, stereoActive, loadCondition).init;
	}

	init {
		fork {
			block { |break|
				synthdefName = decoderName.asSymbol;

				sl.slSynths.decoderLib[synthdefName] ?? {
					break.( warn( synthdefName ++" decoder not found in decoderLib!!!" ))
				};
				// used for introspection by GUI
				attributes = sl.decAttributes.select{|me| me.synthdefName == synthdefName};
				case
				{attributes.size == 1}{
					attributes = attributes[0];
					decType = attributes[\kind];
				}
				{attributes.size == 0}{
					attributes = IdentityDictionary(know: true).put(\kind, \matrix);
					decType = \matrix;
				}
				{attributes.size >1} {
					break.( warn(
						"Found more than one decoder attribute list with that synthdefName: "
						++synthdefName
					))
				};

				server = sl.server;

				sampleRate = server.sampleRate;

				initialBus = stereoActive.if({ inbusnum + 2 }, { inbusnum });

				delayCompBus = CtkAudio(sl.spkrDels.size, server: server);

				case
				{synthdefName.asString.contains("HOA")}
				{
					decSpecs = decSpecs.hoa;

					this.loadHOA
				}
				{this.loadFoaorDiscrete};


			};
			loadCondition.test_(true).signal;
		}
	}

	formatConversion {

	}

	loadFoaorDiscrete {
		case
		{ decType == \discrete } {

			decodersynth = sl.slSynths.decoderLib[synthdefName].note(
				addAction: \head, target: group)
			.in_busnum_(initialBus)
			.out_busnum_(delayCompBus.bus)
		}
		{ sl.curAmbiFormat == 'fuma' } {
			decodersynth = sl.slSynths.decoderLib[synthdefName].note(
				addAction: \head, target: group)
			.in_busnum_(initialBus)
			.out_busnum_(delayCompBus.bus)
			.speakerDistances_(sl.spkrDists)
			.rotate_(if(sl.rotated, {sl.rotateDegree.degrad},{0}))
			.shelfFreq_(sl.shelfFreq)
		}
		{
			formatBus = CtkAudio(4, server: server);
			formatSynth = sl.slSynths.prFormatConvSynth(4, sl.curAmbiFormat, 'fuma', 1).note(addAction: \head, target: group)
			.inbus_(initialBus)
			.outbus_(formatBus.bus);

			decodersynth = sl.slSynths.decoderLib[synthdefName].note(
				addAction: \after, target: formatSynth)
			.in_busnum_(formatBus.bus)
			.out_busnum_(delayCompBus.bus)
			.speakerDistances_(sl.spkrDists)
			.rotate_(if(sl.rotated, {sl.rotateDegree.degrad},{0}))
			.shelfFreq_(sl.shelfFreq);

		};

		compsynth = sl.slSynths.synthLib[\delay_gain_comp].note(
			addAction: \tail, target: group)
		.in_busnum_(delayCompBus.bus).out_busnum_(outbusnum)
		.xover_hpf_(sl.xOverHPF).xover_lpf_(sl.xOverLPF)
		.masterAmp_(sl.globalAmp).speakerDelays_(sl.spkrDels).speakerGains_(sl.spkrGains);
	}

	loadHOA {



		// unpack dec specs
		kernelPath = sl.config.psyFoclKernelPath;
		kernelPath = kernelPath !? {
			if(kernelPath[0].asSymbol == '/', {//it's absolute path
				PathName.new(kernelPath);
			}, {
				if(kernelPath[0] == "~", {//it's relative to home directory
					PathName.new(kernelPath.standardizePath);
				}, {//it's relative to the class file
					PathName.new(File.realpath(this.class.filenameSymbol).dirname ++ "/"
						++ kernelPath)
				});
			});
		};



		satOrder = attributes.satOrder;
		subOrder = attributes.subOrder;

		numBChannels = HoaOrder.new(satOrder).size;
		numSubChannels = HoaOrder.new(subOrder).size;
		convBus = CtkAudio.new(numBChannels + numSubChannels, server: server);

		psyFoclSize = decSpecs.psyFoclSize;
		alpha = decSpecs.alpha;  // kaiser smoothing window
		foclRadius = decSpecs.foclRadius;   // focalisation radius
		mainBeamDict = decSpecs.mainBeamDict;
		mainDim = decSpecs.satDim;
		match = decSpecs.match;
		subBeamDict = decSpecs.subBeamDict;
		subDim = decSpecs.subDim;  // 2D for subs

		psyFoclKernelPath = kernelPath.fullPath ++ sampleRate.asInteger ++ "/";

		[satOrder, psyFoclSize, alpha, foclRadius, mainBeamDict.beamShapes, mainBeamDict.edgeFreqs, mainDim, match, subBeamDict.beamShapes, subDim].do{ |param|
			psyFoclKernelPath = psyFoclKernelPath ++ param ++ "_"
		};

		psyFoclKernelPath = psyFoclKernelPath[0..psyFoclKernelPath.size-2] ++ "/";

		psyFoclKernelPath = PathName(psyFoclKernelPath.replace("[", "").replace("]", "").replace(" ", "").replace(",", "_"));

		irbuffers = [];

		server.sync;

		"Loading FoclPsycho Kernels".postln;

		this.prGeneratePsyFoclKernels;

		psyFoclKernelPath.deepFiles.do{ |pathname|
			(pathname.extension == "wav").if({
				irbuffers = irbuffers.add(CtkBuffer(pathname.fullPath, server: server).load(sync: false))
			})
		};

		server.sync;



		convSynthDef = CtkSynthDef('soundLabHoaConv', {
			arg in_busnum, out_busnum, gate = 1, fadeTime;
			var in, sats, subs, env;
			env = EnvGen.ar(
				Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
				gate, doneAction: 2
			);

			in = In.ar(in_busnum, numBChannels) * env;

			// // sats
			// sats = irbuffers[0..satOrder].collect{|buffer, i|
			// 	var indices;
			// 	indices = i.asHoaDegree.indices;
			// 	Convolution2.ar(in[indices], buffer, framesize: psyFoclSize)
			// }.flatten;
			//
			// // subs
			// subs = irbuffers[satOrder+1..].collect{|buffer, i|
			// 	var indices;
			// 	indices = i.asHoaDegree.indices;
			// 	Convolution2.ar(in[indices], buffer, framesize: psyFoclSize)
			// }.flatten;

			// remove Focal for now
			sats = in;
			subs = in[0..3];

			Out.ar(out_busnum, sats ++ subs)
		});

		server.sync;

		(sl.curAmbiFormat != 'atk').if({

			formatBus = CtkAudio(numBChannels, server: server);
			formatSynth = sl.slSynths.prFormatConvSynth(numBChannels, sl.curAmbiFormat, 'atk', satOrder).note(addAction: \head, target: group)
			.inbus_(initialBus)
			.outbus_(formatBus.bus);

			convSynth = convSynthDef.note(
				addAction: \after, target: formatSynth)
			.in_busnum_(formatBus.bus)
			.out_busnum_(convBus.bus);

		}, {
			convSynth = convSynthDef.note(
				addAction: \head, target: group)
			.in_busnum_(initialBus)
			.out_busnum_(convBus.bus);

		});

		decodersynth = sl.slSynths.decoderLib[synthdefName].note(
			addAction: \tail, target: group)
		.in_busnum_(convBus.bus)
		.rotate_(if(sl.rotated, {sl.rotateDegree.degrad},{0}))
		.out_busnum_(delayCompBus.bus)
		.speakerDistances_(sl.spkrDists);



		compsynth = sl.slSynths.synthLib[\delay_gain_comp].note(
			addAction: \after, target: decodersynth)
		.in_busnum_(delayCompBus.bus).out_busnum_(outbusnum)
		.xover_hpf_(sl.xOverHPF).xover_lpf_(sl.xOverLPF)
		.masterAmp_(sl.globalAmp).speakerDelays_(sl.spkrDels).speakerGains_(sl.spkrGains);

	}

	prGeneratePsyFoclKernels {

		psyFoclKernelPath.isFolder.not.if({

			psyFoclKernelPath.fullPath.mkdir;

			// design kernels
			mainPsychoKernels = Signal.hoaMultiBandFocl(
				size: psyFoclSize,
				radius: foclRadius,
				beamDict: mainBeamDict,
				dim: mainDim,
				match: match,
				order: satOrder,
				sampleRate: sampleRate
			);

			mainPsychoKernels = mainPsychoKernels.collect({ |kernel|
				Signal.kaiserWindow(psyFoclSize, a: alpha) * kernel // window
			});

			subPsychoKernels = Signal.hoaMultiBandFocl(
				size: psyFoclSize,
				radius: foclRadius,
				beamDict: subBeamDict,
				dim: subDim,
				match: match,
				order: subOrder,
				sampleRate: sampleRate
			);

			subPsychoKernels = subPsychoKernels.collect({ |kernel| Signal.kaiserWindow(psyFoclSize, a: alpha) * kernel });  // window

			(mainPsychoKernels ++ subPsychoKernels).do({ |kernel, i|
				kernel.write(
					path: psyFoclKernelPath.fullPath ++ "FIR_" ++ i.asString.padLeft(2, "0") ++ ".wav",
					headerFormat: "WAV",
					sampleFormat: "float",
					sampleRate: sampleRate
				)
			})  // as a sound file

		})
	}

	shelfFreq_ { |frq|
		if( decType != \discrete, { decodersynth.shelfFreq_(frq) });
	}

	play { |xfade = 0.2|
		formatSynth !? { formatSynth.play };
		decodersynth.fadeTime_(xfade).play;
		compsynth.play;
		convSynth !? {convSynth.fadeTime_(xfade).play}
	}

	pause {
		decodersynth.pause;
		compsynth.pause;
		convSynth !? {convSynth.pause}
	}

	unPause {
		decodersynth.run;
		compsynth.run;
		convSynth !? {convSynth.run}
	}

	free { |xfade = 0.2, cond|
		fork {
			formatSynth !? { formatSynth.free };
			convSynth !? {convSynth.fadeTime_(xfade).release};
			decodersynth.fadeTime_(xfade).release;
			xfade.wait;
			compsynth.free;
			convBus !? { convBus.free };
			delayCompBus.free;
			cond !? { cond.test_(true).signal }
		}
	}

	routeForStereo { |bool|
		(stereoActive == bool).if({ "No change in Stereo".postln }, {
			stereoActive = bool;
			bool.if({
				"Routed for Stereo!".postln;
				convSynth.notNil.if({convSynth.in_busnum_(inbusnum + 2)}, {decodersynth.in_busnum_(inbusnum + 2)})
			}, {
				"Stereo Cleared!".postln;
				convSynth.notNil.if({convSynth.in_busnum_(inbusnum)}, {decodersynth.in_busnum_(inbusnum)})
			})
		})

	}

	// NOTE: this changes with the stereo setting, so ask the synth
	inbusnum { ^convSynth.notNil.if({ convSynth.in_busnum }, { decodersynth.in_busnum }) }
	outbusnum { ^compsynth.out_busnum }

	outbusnum_ { |bus| compsynth.out_busnum_(bus) }

	kernelUpdate {
		decodersynth.speakerDistances_(sl.spkrDists);
		compsynth.speakerDelays_(sl.spkrDels).speakerGains_(sl.spkrGains)
	}
}