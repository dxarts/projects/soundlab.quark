SoundLabSynthswFOA {
	var <sl;
	var <decoderLib, <synthLib;

	*new { |soundLab|
		^super.newCopyArgs(soundLab).init
	}

	init {
		decoderLib = CtkProtoNotes();
		synthLib = CtkProtoNotes()
	}

	loadSynths {
		sl.decAttributes.do{ |decSpecs|
			switch( decSpecs.kind,
				\diametric,	{ this.prLoadDiametricDecoderSynth(decSpecs) },
				\dome,		{ this.prLoadDiametricDomeDecoderSynth(decSpecs) },
				\discrete,	{ this.prLoadDiscreteRoutingSynth(decSpecs)	},
				\hoa,       { this.prLoadHoaDecoderSynth(decSpecs) },
				\hoamatrix, { this.prLoadHoaMatrixDecoderSynth(decSpecs) },
				\hoaPantoAllrad, { this.prLoadHoaPantoAllradDecoderSynth(decSpecs) }
			);
		};
		this.prDelayGainComp;
	}

	prLoadDiametricDecoderSynth { |decSpecs|
		var arrayOutIndices, satOutbusNums, subOutbusNums, satDirections, subDirections;
		var matrix_dec_sat, matrix_dec_sub, decSynthDef;


		/* --satellites matrix-- */

		arrayOutIndices = decSpecs.arrayOutIndices;

		// get the other half of array indices for diametric opposites
		satOutbusNums = arrayOutIndices
		++ arrayOutIndices.collect({ |spkdex| sl.spkrOppDict[spkdex] });

		// only need to provide 1/2 of the directions for diametric decoder
		satDirections = arrayOutIndices.collect({|busnum|
			switch(decSpecs.dimensions,
				2, sl.spkrDirs[busnum][0], // 2D
				3, sl.spkrDirs[busnum]     // 3D
			);
		});

		matrix_dec_sat = FoaDecoderMatrix.newDiametric(satDirections, decSpecs.k).shelfFreq_(sl.shelfFreq);


		/* --subs matrix-- */

		// always use all the subs
		subOutbusNums = (sl.numSatChans..(sl.numSatChans+sl.numSubChans-1));

		// prepare stereo decoder for subs or diammetric if there's an even number of them > 2
		if(sl.numSubChans.even, {
			// only need to provide 1/2 of the directions for diametric decoder
			subDirections = subOutbusNums.keep( (subOutbusNums.size/2).asInteger ).collect({
				|busnum|
				sl.spkrDirs[busnum][0]  // subs always 2D
			});

			matrix_dec_sub = (subDirections.size > 1).if(
				{ FoaDecoderMatrix.newDiametric(subDirections, decSpecs.k).shelfFreq_(sl.shelfFreq);
				},
				// stereo decoder for 2 subs, symmetrical across x axis, cardioid decode
				{ FoaDecoderMatrix.newStereo(subDirections[0], 0.5).shelfFreq_(sl.shelfFreq) }
			)
		});


		/* --build the synthdef-- */

		decSynthDef = SynthDef( decSpecs.synthdefName, {
			arg out_busnum=0, in_busnum, fadeTime=0.2, subgain=0, rotate=0, gate=1;
			var in, env, sat_out, sub_out, spDists;

			spDists = \speakerDistances.ir(sl.spkrDists);

			env = EnvGen.ar(
				Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
				gate, doneAction: 2
			);
			in = In.ar(in_busnum, decSpecs.numInputChans) * env; // B-Format signal
			in = FoaTransform.ar(in, 'rotate', rotate); // rotate the listening orientation

			// include shelf filter if the satellite
			// matrix has a shelf freq specified
			if( matrix_dec_sat.shelfFreq.isNumber, {
				in = FoaPsychoShelf.ar(
					in,
					//matrix_dec_sat.shelfFreq,
					\shelfFreq.kr(matrix_dec_sat.shelfFreq), // see Control.names
					matrix_dec_sat.shelfK.at(0),
					matrix_dec_sat.shelfK.at(1)
				)
			});

			/* -- sat decode --*/

			// near-field compensate, decode, remap to rig
			// NOTE: adding numbers to CtkAudio is not recommended, for out_busnum, pass the acutal bus number and not the CtkAudio instance
			satOutbusNums.do({ | spkdex, i |
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					AtkMatrixMix.ar(
						FoaNFC.ar( in, spDists.at(spkdex).abs ), // NOTE .abs in case of negative distance
						matrix_dec_sat.matrix.fromRow(i)
					)
					* decSpecs.decGain.dbamp
				)
			});

			/* -- sub decode --*/

			if( sl.numSubChans.even,
				{
					subOutbusNums.do({ | spkdex, i |
						Out.ar(
							out_busnum + spkdex,
							AtkMatrixMix.ar(
								FoaNFC.ar( in, spDists.at(spkdex).abs ),
								matrix_dec_sub.matrix.fromRow(i)
							)
							* subgain.dbamp
							* decSpecs.decGain.dbamp
						)
					})
				},
				// TODO:	this is a quick fix for non-even/non-diametric sub layout
				// 			Note this likely hasn't been used/tested because 113 specifies
				//			a false 2nd sub (i.e. always even)- note for single sub receiving W, boost by 3dB
				{
					if( sl.numSubChans == 1,
						{   // No NFC for 1 sub
							Out.ar(
								out_busnum + subOutbusNums[0],
								// send W to subs, scaled by 3db, div by num of subs
								in[0] * 2.sqrt
								* subgain.dbamp
								* decSpecs.decGain.dbamp
							)
						},
						{
							subOutbusNums.do({ | spkdex, i |
								var nfc;
								nfc = FoaNFC.ar( in, spDists.at(spkdex).abs );
								Out.ar(
									out_busnum + spkdex,
									// send W to subs, div by num of subs
									nfc[0] * sl.numSubChans.reciprocal
									* subgain.dbamp
									* decSpecs.decGain.dbamp
								)
							})
					})
				}
			);
		});

		decoderLib.add( decSynthDef ); // add the synth to the decoder library
		postf("% (diametric) added.\n", decSpecs.synthdefName);
	}

	prLoadHoaDecoderSynth { |decSpecs|
		var arrayOutIndices, satOutbusNums, subOutbusNums, satDirections, subDirections;
		var matrix_dec_sat, matrix_dec_sub, decSynthDef, satOrder, subOrder;

		/* --satellites matrix-- */

		satOutbusNums = decSpecs.arrayOutIndices;

		satOrder = decSpecs.satOrder;

		subOrder = decSpecs.subOrder;

		satDirections = satOutbusNums.collect({|busnum|
			switch(decSpecs.dimensions,
				2, sl.spkrDirs[busnum][0], // 2D
				3, sl.spkrDirs[busnum]     // 3D
			);
		});

		matrix_dec_sat = HoaMatrixDecoder.newModeMatch(satDirections, order: satOrder);

		/* --subs matrix-- */

		// always use all the subs
		subOutbusNums = (sl.numSatChans..(sl.numSatChans+sl.numSubChans-1));

		subDirections = subOutbusNums.collect({
			|busnum|
			sl.spkrDirs[busnum] // subs always 2D
		});

		// design mode matching matrix
		// NOTE: loudspeakers should be relatively evenly distributed
		//
		// beamShape: \basic, match: \amp

		if(sl.numSubChans > 1, {
			matrix_dec_sub = HoaMatrixDecoder.newDirections(subDirections, order: subOrder)
		});


		/* --build the synthdef-- */

		decSynthDef = SynthDef( decSpecs.synthdefName, {
			arg out_busnum=0, in_busnum, fadeTime=0.2, subgain=0, rotate=0, gate=1;
			var in, subIn, env, sat_out, sub_out, spDists;

			spDists = \speakerDistances.ir(sl.spkrDists);

			\shelfFreq.kr(0); // dummy shelfFreq arg for now...

			env = EnvGen.ar(
				Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
				gate, doneAction: 2
			);
			in = In.ar(in_busnum, decSpecs.numInputChans) * env; // Hoa3 Sats
			subIn = In.ar(in_busnum + decSpecs.numInputChans, subOrder.asHoaOrder.size) * env; // Hoa1 Subs
			in = HoaRotate.ar(in, rotate, satOrder); // rotate the listening orientatio
			subIn = HoaRotate.ar(subIn, rotate, subOrder); // rotate the listening orientatio

			/* -- sat decode --*/

			// near-field compensate, decode, remap to rig
			// NOTE: adding numbers to CtkAudio is not recommended, for out_busnum, pass the acutal bus number and not the CtkAudio instance
			satOutbusNums.do({ | spkdex, i |
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					(HoaNFCtrl.ar( in, AtkHoa.refRadius,
						spDists.at(spkdex).abs, satOrder ) // NOTE .abs in case of negative distance
					* matrix_dec_sat.matrix.getRow(i)).sum

					* decSpecs.decGain.dbamp
				)
			});

			/* -- sub decode --*/
			matrix_dec_sub.notNil.if({
				subOutbusNums.do({ | spkdex, i |
					Out.ar(
						out_busnum + spkdex,
						(HoaNFCtrl.ar( subIn, AtkHoa.refRadius,
							spDists.at(spkdex).abs, subOrder ) *
						matrix_dec_sub.matrix.getRow(i)).sum
						* subgain.dbamp
						* decSpecs.decGain.dbamp
					)
				})
			}, {
				Out.ar(
					out_busnum + subOutbusNums[0],
					subIn[0] * 2.sqrt // send W scaled by 3db
					* subgain.dbamp
					* decSpecs.decGain.dbamp
				)
			})
		});

		decoderLib.add( decSynthDef ); // add the synth to the decoder library
		postf("% (diametric) added.\n", decSpecs.synthdefName);
	}

	prLoadHoaMatrixDecoderSynth { |decSpecs|
		var arrayOutIndices, satOutbusNums, subOutbusNums, satDirections, subDirections;
		var matrix_dec_sat, matrix_dec_sub, decSynthDef, satOrder, subOrder;

		/* --satellites matrix-- */

		satOutbusNums = decSpecs.arrayOutIndices;

		satOrder = decSpecs.satOrder;

		subOrder = decSpecs.subOrder;

		satDirections = satOutbusNums.collect({|busnum| sl.spkrDirs[busnum]});

		matrix_dec_sat = HoaMatrixDecoder.newFromFile(decSpecs.matrixFilename, order: satOrder);

		/* --subs matrix-- */

		// always use all the subs
		subOutbusNums = (sl.numSatChans..(sl.numSatChans+sl.numSubChans-1));

		subDirections = subOutbusNums.collect({|busnum| sl.spkrDirs[busnum]});

		// design mode matching matrix
		// NOTE: loudspeakers should be relatively evenly distributed
		//
		// beamShape: \basic, match: \amp

		if(sl.numSubChans > 1, {
			matrix_dec_sub = HoaMatrixDecoder.newDirections(subDirections, order: subOrder)
		});

		/* --build the synthdef-- */

		decSynthDef = SynthDef( decSpecs.synthdefName, {
			arg out_busnum=0, in_busnum, fadeTime=0.2, subgain=0, rotate=0, gate=1;
			var in, subIn, env, sat_out, sub_out, spDists;

			spDists = \speakerDistances.ir(sl.spkrDists);

			\shelfFreq.kr(0); // dummy shelfFreq arg for now...

			env = EnvGen.ar(
				Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
				gate, doneAction: 2
			);
			in = In.ar(in_busnum, decSpecs.numInputChans) * env; // Hoa Sats
			subIn = In.ar(in_busnum + decSpecs.numInputChans, subOrder.asHoaOrder.size) * env; // Hoa1 Subs
			in = HoaRotate.ar(in, rotate, satOrder); // rotate the listening orientation
			subIn = HoaRotate.ar(subIn, rotate, subOrder); // rotate the listening orientation

			/* -- sat decode --*/

			// near-field compensate, decode, remap to rig
			// NOTE: adding numbers to CtkAudio is not recommended, for out_busnum, pass the acutal bus number and not the CtkAudio instance
			satOutbusNums.do({ | spkdex, i |
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					(HoaNFCtrl.ar( in, AtkHoa.refRadius,
						spDists.at(spkdex).abs, satOrder ) // NOTE .abs in case of negative distance
					* matrix_dec_sat.matrix.getRow(i)).sum

					* decSpecs.decGain.dbamp
				)
			});

			/* -- sub decode --*/
			/* -- sub decode --*/
			matrix_dec_sub.notNil.if({
				subOutbusNums.do({ | spkdex, i |
					Out.ar(
						out_busnum + spkdex,
						(HoaNFCtrl.ar( subIn, AtkHoa.refRadius,
							spDists.at(spkdex).abs, subOrder ) *
						matrix_dec_sub.matrix.getRow(i)).sum
						* subgain.dbamp
						* decSpecs.decGain.dbamp
					)
				})
			}, {
				Out.ar(
					out_busnum + subOutbusNums[0],
					subIn[0] * 2.sqrt // send W scaled by 3db
					* subgain.dbamp
					* decSpecs.decGain.dbamp
				)
			})

		});

		decoderLib.add( decSynthDef ); // add the synth to the decoder library
		postf("% (diametric) added.\n", decSpecs.synthdefName);
	}

	prLoadHoaPantoAllradDecoderSynth { |decSpecs|
		var arrayOutIndices, satOutbusNums, subOutbusNums, satDirections, subDirections;
		var matrix_dec_sat, matrix_dec_sub, decSynthDef, satOrder, subOrder;

		/* --satellites matrix-- */

		satOutbusNums = decSpecs.satOuts;

		satOrder = decSpecs.satOrder;

		subOrder = decSpecs.subOrder;

		satDirections = satOutbusNums.collect({|busnum| sl.spkrDirs[busnum]});

		matrix_dec_sat = HoaMatrixDecoder.newPantoAllrad(satDirections, 360, 'vertex', 'energy', 'energy', satOrder);

		/* --subs matrix-- */

		// always use all the subs
		subOutbusNums = (sl.numSatChans..(sl.numSatChans+sl.numSubChans-1));

		subDirections = subOutbusNums.collect({|busnum| sl.spkrDirs[busnum]});

		// design mode matching matrix
		// NOTE: loudspeakers should be relatively evenly distributed
		//
		// beamShape: \basic, match: \amp

		if(sl.numSubChans > 1, {
			matrix_dec_sub = HoaMatrixDecoder.newDirections(subDirections, order: subOrder)
		});

		/* --build the synthdef-- */

		decSynthDef = SynthDef( decSpecs.synthdefName, {
			arg out_busnum=0, in_busnum, fadeTime=0.2, subgain=0, rotate=0, gate=1;
			var in, subIn, env, sat_out, sub_out, spDists;

			spDists = \speakerDistances.ir(sl.spkrDists);

			\shelfFreq.kr(0); // dummy shelfFreq arg for now...

			env = EnvGen.ar(
				Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
				gate, doneAction: 2
			);
			in = In.ar(in_busnum, decSpecs.numInputChans) * env; // Hoa Sats
			subIn = In.ar(in_busnum + decSpecs.numInputChans, subOrder.asHoaOrder.size) * env; // Hoa1 Subs
			in = HoaRotate.ar(in, rotate, satOrder); // rotate the listening orientation
			subIn = HoaRotate.ar(subIn, rotate, subOrder); // rotate the listening orientation

			/* -- sat decode --*/

			// near-field compensate, decode, remap to rig
			// NOTE: adding numbers to CtkAudio is not recommended, for out_busnum, pass the acutal bus number and not the CtkAudio instance
			satOutbusNums.do({ | spkdex, i |
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					(HoaNFCtrl.ar( in, AtkHoa.refRadius,
						spDists.at(spkdex).abs, satOrder ) // NOTE .abs in case of negative distance
					* matrix_dec_sat.matrix.getRow(i)).sum

					* decSpecs.decGain.dbamp
				)
			});

			/* -- sub decode --*/
			/* -- sub decode --*/
			matrix_dec_sub.notNil.if({
				subOutbusNums.do({ | spkdex, i |
					Out.ar(
						out_busnum + spkdex,
						(HoaNFCtrl.ar( subIn, AtkHoa.refRadius,
							spDists.at(spkdex).abs, subOrder ) *
						matrix_dec_sub.matrix.getRow(i)).sum
						* subgain.dbamp
						* decSpecs.decGain.dbamp
					)
				})
			}, {
				Out.ar(
					out_busnum + subOutbusNums[0],
					subIn[0] * 2.sqrt // send W scaled by 3db
					* subgain.dbamp
					* decSpecs.decGain.dbamp
				)
			})

		});

		decoderLib.add( decSynthDef ); // add the synth to the decoder library
		postf("% (diametric) added.\n", decSpecs.synthdefName);
	}

	// NOTE: arrayOutIndices is [half of horiz] ++ [all elevation dome] spkr indices
	prLoadDiametricDomeDecoderSynth { |decSpecs|
		var domeOutbusNums, domeOutbusNumsFullHoriz, partialDomeDirections, subOutbusNums, subDirections;
		var halfHorizDirections, posElevDirections, halfSphereDirections, lowerStartDex, domeEndDex, domeDecoderMatrix;
		var sphereDecoderMatrix, subDecoderMatrix, decSynthDef;
		var lowerMatrix, lowerSum, lowerComp;
		var lowerK = -8.0.dbamp;

		/* --dome satellites-- */

		domeOutbusNums = decSpecs.arrayOutIndices; // half horiz & full dome spkr indices

		// append other half of horiz outbus nums for collecting matrix outputs below
		// select busnums with 0 elevation then collect their opposite's busnum
		domeOutbusNumsFullHoriz = domeOutbusNums
		++ domeOutbusNums.select({|busnum| sl.spkrDirs[busnum][1]==0 }).collect({
			|spkdex| sl.spkrOppDict[spkdex]
		});

		partialDomeDirections = domeOutbusNums.collect({|busnum| sl.spkrDirs[busnum] });
		halfHorizDirections = partialDomeDirections.select{|item| item[1]==0 };
		posElevDirections = partialDomeDirections.select{|item| item[1]>0 };
		halfSphereDirections = halfHorizDirections ++ posElevDirections;

		// model full diametric decoder, and matrix
		sphereDecoderMatrix = FoaDecoderMatrix.newDiametric(halfSphereDirections, decSpecs.k).shelfFreq_(sl.shelfFreq);

		// truncate to just lower speakers to calculate compensation matrix...
		lowerStartDex = (halfHorizDirections.size*2) + posElevDirections.size;

		lowerMatrix = Matrix.with(sphereDecoderMatrix.matrix.asArray[lowerStartDex..]);
		lowerSum = (lowerK / posElevDirections.size) * lowerMatrix.sumCols;
		lowerComp = Matrix.with(
			Array.fill(halfHorizDirections.size,{lowerSum})		// add to first half of horiz
			++ Array.fill2D(posElevDirections.size,4,{0})		// add 0 to elevation spkrs
			++ Array.fill(halfHorizDirections.size,{lowerSum})	// add to second half of horiz
		);

		// truncate - to decoding matrix (raw matrix).. and add compensation matrix
		// note final matrix speaker order will be:
		// 		first half of horizontal speakers,
		// 		positive-elevation dome speakers,
		//		seccond half of horizontal speakers, opposites in same order of the first half
		domeEndDex = lowerStartDex - 1;
		// NOTE: this is a Matrix object, not an FoaDecoderMatrix object
		domeDecoderMatrix = Matrix.with(sphereDecoderMatrix.matrix.asArray[..domeEndDex]);
		domeDecoderMatrix = domeDecoderMatrix + lowerComp;

		/*----------*/
		/* --subs-- */
		/*----------*/
		// always use all the subs
		subOutbusNums = (sl.numSatChans..(sl.numSatChans+sl.numSubChans-1));

		// prepare stereo or diammetric decoder for subs if there's an even number of them
		if(sl.numSubChans.even, {
			// only need to provide 1/2 of the directions for diametric decoder
			subDirections = subOutbusNums.keep( (subOutbusNums.size/2).asInteger ).collect({
				|busnum|
				sl.spkrDirs[busnum][0]  // subs always 2D
			});
			// note subDirections is only half of the subs
			subDecoderMatrix = (subDirections.size > 1).if(
				{ FoaDecoderMatrix.newDiametric(subDirections, decSpecs.k).shelfFreq_(sl.shelfFreq) },
				// stereo decoder for 2 subs, symmetrical across x axis, cardioid decode
				{ FoaDecoderMatrix.newStereo(subDirections[0], 0.5).shelfFreq_(sl.shelfFreq) }
			)
		});

		/*------------------------*/
		/* --build the synthdef-- */
		/*------------------------*/
		decSynthDef = SynthDef( decSpecs.synthdefName, {
			arg out_busnum=0, in_busnum, fadeTime=0.2, subgain=0, rotate=0, gate=1;
			var in, env, sat_out, sub_out, spDists;

			spDists = \speakerDistances.ir(sl.spkrDists);

			env = EnvGen.ar(
				Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
				gate, doneAction: 2
			);

			in = In.ar(in_busnum, decSpecs.numInputChans) * env; // B-Format signal
			in = FoaTransform.ar(in, 'rotate', rotate); // rotate the listening orientation

			// include shelf filter on inpput or not. inferred from sphere FoaDecoderMatrix
			// because domeDecoderMatrix is actually just a Matrix object (no .shelfFreq)
			if( sphereDecoderMatrix.shelfFreq.isNumber, {
				in = FoaPsychoShelf.ar(
					in,
					//sphereDecoderMatrix.shelfFreq,
					\shelfFreq.kr(sphereDecoderMatrix.shelfFreq),
					sphereDecoderMatrix.shelfK.at(0),
					sphereDecoderMatrix.shelfK.at(1)
				)
			});

			// near-field compensate, decode, remap to rig
			// NOTE: adding numbers to CtkAudio is not recommended, for out_busnum, pass the acutal bus number and not the CtkAudio instance
			domeOutbusNumsFullHoriz.do({ |spkdex, i|
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					AtkMatrixMix.ar(
						FoaNFC.ar( in, spDists.at(spkdex).abs ),
						domeDecoderMatrix.fromRow(i)
					)
					* decSpecs.decGain.dbamp
				)
			});

			// sub decode
			if( sl.numSubChans.even, {
				subOutbusNums.do({ |spkdex, i|
					Out.ar(
						out_busnum + spkdex,
						AtkMatrixMix.ar(
							FoaNFC.ar( in, spDists.at(spkdex).abs ),
							subDecoderMatrix.matrix.fromRow(i)
						)
						* subgain.dbamp
						* decSpecs.decGain.dbamp
					)
				})
				// quick fix for non-even/non-diametric sub layout
			},{
				if( sl.numSubChans == 1,
					{   // No NFC for 1 sub
						Out.ar(
							out_busnum + subOutbusNums[0],
							// send W to subs, scaled by 3db, div by num of subs
							in[0] * 2.sqrt
							* subgain.dbamp
							* decSpecs.decGain.dbamp
						)
					},
					{
						// TODO: for odd multi-channel sub layouts
						// build panto decoder
						subOutbusNums.do({ | spkdex, i |
							var nfc;
							nfc = FoaNFC.ar( in, spDists.at(spkdex).abs );
							Out.ar(
								out_busnum + spkdex,
								// send W to subs, scaled by 3db, div by num of subs
								nfc[0] * sl.numSubChans.reciprocal
								* subgain.dbamp
								* decSpecs.decGain.dbamp
							)
						})
				})
			}
			);
		});

		// add the synth to the decoder library
		decoderLib.add( decSynthDef );
		postf("% (dome) added.\n", decSpecs.synthdefName);
	}


	prLoadSingleMatrixDecoder { |matrixPN|
		var subOutbusNums, subDirections, subDecoderMatrix;
		var path, name, matrix, ambiOrder, decSynthDef;


		/* --load decoder coefficient matrix-- */

		path = matrixPN.fullPath;
		name = matrixPN.fileNameWithoutExtension.asSymbol;

		matrix = Matrix.with(FileReader.read(path).asFloat);
		// determine order from matrix (must be 'full' order)
		ambiOrder = matrix.cols.sqrt.asInteger - 1;

		postf("Loading matrix decoder:\t\t\t%, order %\n", name, ambiOrder);


		/* --subs-- */

		// always use all the subs
		subOutbusNums = (sl.numSatChans..(sl.numSatChans+sl.numSubChans-1));

		// prepare stereo or diammetric decoder for subs if there's an even number of them
		// assume the layout is regular in this case
		if(sl.numSubChans.even, {
			// only need to provide 1/2 of the directions for diametric decoder
			subDirections = subOutbusNums.keep( (subOutbusNums.size/2).asInteger ).collect({
				|busnum|
				sl.spkrDirs[busnum][0]  // subs always 2D
			});
			// note subDirections is only half of the subs
			subDecoderMatrix = (subDirections.size > 1).if(
				{ FoaDecoderMatrix.newDiametric(subDirections).shelfFreq_(sl.shelfFreq) },
				// stereo decoder for 2 subs, symmetrical across x axis, cardioid decode
				{ FoaDecoderMatrix.newStereo(subDirections[0], 0.5).shelfFreq_(sl.shelfFreq) }
			)
		});


		/* --build the synthdef-- */

		decSynthDef = SynthDef( name, {
			arg out_busnum=0, in_busnum, fadeTime=0.2, subgain=0, rotate=0, freq=400, gate=1,
			shelfFreq = 400;  // shelfFreq defined but not used in single matrix decoder
			var in, env, sat_out, sub_out, spDists;

			spDists = \speakerDistances.ir(sl.spkrDists);

			env = EnvGen.ar(
				Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
				gate, doneAction: 2
			);

			in = In.ar(in_busnum, (ambiOrder + 1).squared) * env;
			(ambiOrder == 1).if{ // transform only supported at first order atm
				in = FoaTransform.ar(in, 'rotate', rotate); // rotate the listening orientation
			};

			// near-field compensate, decode, remap to rig
			// it's expected that the matrix has outputs for all speakers in the rig,
			// even if some are zeroed out in the matrix
			// NOTE: adding numbers to CtkAudio is not recommended, for out_busnum, pass the acutal bus number and not the CtkAudio instance
			sl.numSatChans.do({ | spkdex, i |
				var nfc;
				(ambiOrder == 1).if( // nfc only supported at first order atm
					{ nfc = FoaNFC.ar( in, spDists.at(spkdex).abs ) },
					{ nfc = in }
				);
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					AtkMatrixMix.ar(
						nfc, matrix.fromRow(i)
					)
				)
			});

			// sub decode
			if( sl.numSubChans.even, {
				subOutbusNums.do({ |spkdex, i|
					var nfc;
					(ambiOrder == 1).if( // nfc only supported at first order atm
						{ nfc = FoaNFC.ar( in, spDists.at(spkdex).abs ) },
						{ nfc = in }
					);
					Out.ar(
						out_busnum + spkdex,
						AtkMatrixMix.ar(
							nfc, subDecoderMatrix.matrix.fromRow(i)
						) * subgain.dbamp
					)
				})
				// quick fix for non-even/non-diametric sub layout
			},{
				if( sl.numSubChans == 1,
					{   // No NFC for 1 sub
						Out.ar(
							out_busnum + subOutbusNums[0],
							// send W to subs, scaled by 3db, div by num of subs
							in[0] * 2.sqrt
						) * subgain.dbamp
					},
					{
						subOutbusNums.do({ | spkdex, i |
							var nfc;
							(ambiOrder == 1).if( // nfc only supported at first order atm
								{ nfc = FoaNFC.ar( in, spDists.at(spkdex).abs ) },
								{ nfc = in }
							);
							Out.ar(
								out_busnum + spkdex,
								// send W to subs, scaled by 3db, div by num of subs
								nfc[0] * sl.numSubChans.reciprocal
							) * subgain.dbamp
						})
				})
			}
			);

		});

		// add the synth to the decoder library
		decoderLib.add( decSynthDef );
		sl.matrixDecoderNames = sl.matrixDecoderNames.add(name);
	}

	prLoadDualMatrixDecoder { |decName, matrixPN_LF, matrixPN_HF|
		var subOutbusNums, subDirections, subDecoderMatrix;
		var lf_array, hf_array;
		var path_lf, path_hf, name, matrix_lf, matrix_hf, ambiOrder, decSynthDef;

		/*-------------------------------------*/
		/* --load decoder coefficient matrix-- */
		/*-------------------------------------*/
		path_lf = matrixPN_LF.fullPath;
		path_hf = matrixPN_HF.fullPath;
		name = decName.asSymbol;

		lf_array = FileReader.read(path_lf).asFloat;
		hf_array = FileReader.read(path_hf).asFloat;

		// load decoder coefficient matrix
		matrix_lf = Matrix.with(lf_array);
		matrix_hf = Matrix.with(hf_array);

		// determine order from matrix (must be 'full' order)
		// NOTE: addition of matricies is a quick way to check whether they are the same
		ambiOrder = (matrix_lf + matrix_hf).cols.sqrt.asInteger - 1;

		postf("Loading dual matrix decoder:\t%, order %\n", name, ambiOrder);

		/*----------*/
		/* --subs-- */
		/*----------*/
		// always use all the subs
		subOutbusNums = (sl.numSatChans..(sl.numSatChans+sl.numSubChans-1));

		// prepare stereo or diammetric decoder for subs if there's an even number of them
		// assume the layout is regular in this case
		if(sl.numSubChans.even, {
			// only need to provide 1/2 of the directions for diametric decoder
			subDirections = subOutbusNums.keep( (subOutbusNums.size/2).asInteger ).collect({
				|busnum|
				sl.spkrDirs[busnum][0]  // subs always 2D
			});
			// note subDirections is only half of the subs
			subDecoderMatrix = (subDirections.size > 1).if(
				{ FoaDecoderMatrix.newDiametric(subDirections).shelfFreq_(sl.shelfFreq) },
				// stereo decoder for 2 subs, symmetrical across x axis, cardioid decode
				{ FoaDecoderMatrix.newStereo(subDirections[0], 0.5).shelfFreq_(sl.shelfFreq) }
			)
		});

		/*------------------------*/
		/* --build the synthdef-- */
		/*------------------------*/
		decSynthDef = SynthDef( name, {
			arg out_busnum=0, in_busnum, fadeTime=0.2, subgain=0, rotate=0, shelfFreq=400, gate=1;
			var in, env, sat_out, sub_out, spDists;
			var k = -180.dbamp; // RM-shelf gain (for cross-over)

			spDists = \speakerDistances.ir(sl.spkrDists);

			env = EnvGen.ar(
				Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
				gate, doneAction: 2
			);

			// read in (ambisonic 'b-format')
			in = In.ar(in_busnum, (ambiOrder + 1).squared) * env;

			// transform and physcoshelf only supported at first order atm
			(ambiOrder == 1).if{
				in = FoaTransform.ar(in, 'rotate', rotate); // rotate the listening orientation
			};

			// near-field compensate, decode, remap to rig
			// it's expected that the matrix has outputs for all speakers in the rig,
			// even if some are zeroed out in the matrix
			// NOTE: adding numbers to CtkAudio is not recommended, for out_busnum, pass the acutal bus number and not the CtkAudio instance
			sl.numSatChans.do({ | spkdex, i |
				var nfc;

				(ambiOrder == 1).if( // nfc only supported at first order atm
					{ nfc = FoaNFC.ar( in, spDists.at(spkdex).abs ) },
					{ nfc = in }
				);
				// LF
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					AtkMatrixMix.ar(
						RMShelf2.ar( nfc, sl.shelfFreq, k ),
						matrix_lf.fromRow(i)
					)
				);
				// HF
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					AtkMatrixMix.ar(
						RMShelf2.ar( nfc, sl.shelfFreq, -1 * k ),
						matrix_hf.fromRow(i)
					)
				);
			});

			// sub decode
			if( sl.numSubChans.even, {

				subOutbusNums.do({ |spkdex, i|
					var nfc;
					(ambiOrder == 1).if( // nfc only supported at first order atm
						{ nfc = FoaNFC.ar( in, spDists.at(spkdex) ) },
						{ nfc = in }
					);
					Out.ar(
						out_busnum + spkdex,
						AtkMatrixMix.ar(
							nfc, subDecoderMatrix.matrix.fromRow(i)
						) * subgain.dbamp
					);
				})
				// quick fix for non-even/non-diametric sub layout
			},{
				if( sl.numSubChans == 1,
					{   // No NFC for 1 sub
						Out.ar(
							out_busnum + subOutbusNums[0],
							// send W to subs, scaled by 3db, div by num of subs
							in[0] * 2.sqrt
						) * subgain.dbamp
					},
					{
						subOutbusNums.do({ | spkdex, i |
							var nfc;
							(ambiOrder == 1).if( // nfc only supported at first order atm
								{ nfc = FoaNFC.ar( in, spDists.at(spkdex).abs ) },
								{ nfc = in }
							);
							Out.ar(
								out_busnum + spkdex,
								// send W to subs, scaled by 3db, div by num of subs
								nfc[0] * sl.numSubChans.reciprocal
							) * subgain.dbamp
						})
				})
			}
			);

		});

		// add the synth to the decoder library
		decoderLib.add( decSynthDef );
		sl.matrixDecoderNames = sl.matrixDecoderNames.add(name);
	}

	prLoadDiscreteRoutingSynth { |decSpecs|

		decoderLib.add(
			SynthDef( decSpecs.synthdefName, {
				arg in_busnum, out_busnum = 0, fadeTime = 0.3, subgain = 0, gate = 1, rotate = 0;
				var in, env, out;
				var azims, elevs, directions, encoder, bf, decoders, sub_decodes, spDists;

				spDists = \speakerDistances.ir(sl.spkrDists);

				env = EnvGen.ar(
					Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
					gate, doneAction: 2 );

				in = In.ar(in_busnum, decSpecs.numInputChans) * env;
				decSpecs.arrayOutIndices.do{ |outbus, i|
					Out.ar(outbus + out_busnum, in[i] * decSpecs.decGain.dbamp)
				};

				// subs were considered at one point, but decided discrete routing should be
				// direct speaker feeds only
				// // TODO: confirm this BF encode-decode approach
				// // SUBS
				// if( config.sl.numSubChans > 1, {
				// 	// send the satellite signals to the sub(s) via planewave encoding,
				// 	// then decode the b-format to mono sub decoders
				// 	azims = decSpecs.arrayOutIndices.collect{|outdex, i| config.spkrAzimuthsRad[outdex] };
				// 	elevs = decSpecs.arrayOutIndices.collect{|outdex, i| config.spkrElevationsRad[outdex] };
				// 	directions = [azims, elevs].lace(azims.size + elevs.size).clump(2);
				//
				// 	encoder = FoaEncoderMatrix.newDirections(directions, nil); // nil = planewave encoding
				// 	bf = FoaEncode.ar(in, encoder);
				//
				// 	// Mono decode for each sub
				// 	decoders = config.sl.numSubChans.collect{|i|
				// 		FoaDecoderMatrix.newMono(
				// 			config.spkrAzimuthsRad[config.sl.numSatChans+i], // sub azimuth
				// 			0,  // sub elevation - always 2D
				// 			0.5 // cardiod decode
				// 		);
				// 	};
				//
				// 	sub_decodes = decoders.collect{ |decoder| FoaDecode.ar(bf, decoder); };
				// 	// TODO add crossover to discrete routing
				// 	// see commented-out code below for crossover scheme to be added
				// 	config.sl.numSubChans.do{|i| Out.ar(config.sl.numSatChans+i, sub_decodes[i])};
				//
				// 	},{
				// 		if( config.sl.numSubChans == 1, {
				// 			Out.ar( config.sl.numSatChans, Mix.ar(in) * decSpecs.numInputChans.reciprocal )
				// 		})
				// 	}
				// );
			});
		);

		// debug
		postf("% (discrete) added.\n", decSpecs.synthdefName);
	}

	prFormatConvSynth { |numInputs, inputFormat, outputFormat, order|
		^case
		// encode to atk
		{ outputFormat == 'atk' } {
			// assume that both inputs (FuMa and AmbiX) have refRadius = inf
			CtkSynthDef('formatConversion', { |inbus, outbus|
				var input, conversion;
				input = In.ar(inbus, numInputs);
				conversion = HoaEncodeMatrix.ar(
					HoaNFDist.ar(input, order), // convert refRadius to AtkHoa.refRadius
					HoaMatrixEncoder.newFormat(inputFormat, order)
				);
				Out.ar(outbus, conversion)
			})
		}
		// encode from ambix, decode to fuma
		{ outputFormat == 'fuma' and: inputFormat == 'ambix' } {
			// no radius conversion as both assumed to be at refRadius = inf
			CtkSynthDef('formatConversion', { |inbus, outbus|
				var input, conversion;
				input = In.ar(inbus, numInputs);
				conversion = HoaDecodeMatrix.ar(
					HoaEncodeMatrix.ar(input, HoaMatrixEncoder.newFormat(inputFormat, order)),
					HoaMatrixDecoder.newFormat(outputFormat, order)
				);
				Out.ar(outbus, conversion)
			})

		}
		{ outputFormat == 'fuma' and: inputFormat == 'atk' } {
			CtkSynthDef('formatConversion', { |inbus, outbus|
				var input, conversion;
				input = In.ar(inbus, numInputs);
				conversion = HoaDecodeMatrix.ar(
					HoaNFProx.ar(
						HPF.ar(input, 110.0/3), // precondition re: HoaNFProx help file
						order
					),
					HoaMatrixDecoder.newFormat(outputFormat, order)
				);
				Out.ar(outbus, conversion)
			})
		}

	}

	prDelayGainComp {
		synthLib.add(
			SynthDef(\delay_gain_comp, { arg in_busnum=0, out_busnum=0, masterAmp = 1.0, xover_hpf = 60, xover_lpf = 60;
				var in_sig, sat_sig, stereo_sig, sub_sig, subs_xover, sats_xover, subs_delayed, sats_delayed, outs, spDels, spGains;

				spDels = \speakerDelays.ir(sl.spkrDels);

				spGains = \speakerGains.ir(sl.spkrGains);

				sat_sig = In.ar(in_busnum, sl.numSatChans) * spGains[0 .. sl.numSatChans - 1].dbamp;
				sub_sig = In.ar(in_busnum+sl.numSatChans, sl.numSubChans)
				* spGains[sl.numSatChans..(sl.numSatChans+sl.numSubChans-1)].dbamp;
				// stereo_sig = In.ar(in_busnum+totalArrayChans);

				subs_xover = LPF.ar( LPF.ar(sub_sig, xover_lpf), xover_lpf);
				sats_xover = HPF.ar( HPF.ar(sat_sig, xover_hpf), xover_hpf);

				sats_delayed = DelayN.ar( sats_xover,
					sl.spkrDels.maxItem + 0.5, spDels[0..(sl.numSatChans-1)] );
				subs_delayed = DelayN.ar( subs_xover,
					sl.spkrDels.maxItem + 0.5, spDels[sl.numSatChans..(sl.numSatChans+sl.numSubChans-1)] );


				// Note: no stereo delay/gain comp atm
				outs = sats_delayed ++ subs_delayed /*++ stereo_sig */;
				Out.ar(out_busnum, outs * masterAmp);
			})
		)
	}
}