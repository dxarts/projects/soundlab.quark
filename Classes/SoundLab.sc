// TODO:
// ~~ create a distinction between the functionality that uses PartConv (usingKernels boolean)
// and simply doing \basic_balance correction (which doesn't use convolution), so the \basic_balance
// isn't labeled as a "kernel", i.e. curKernel ~~

SoundLab {
	// copyArgs
	var <configFileName, <>usingKernels, <loadGUI, <usingSLHW;

	var <>xfade = 0.2,  <>debug=true, <kernels;
	var <globalAmp, <numSatChans, <numSubChans, <totalArrayChans, <numKernelChans, <>rotateDegree, <>shelfFreq;
	var <hwInCount, <hwInStart, <hwOutStart, <firstInputArr, firstOutputArr;
	var <config, <labName, <numHardwareOuts, <numHardwareIns, <stereoChanIndex, <stereoSubChanIndex, <>defaultDecoderName, <>kernelDirPathName, <>decoderMatricesPath, <configRelativePathName = "../config/", <initSR;
	var <thisStereoChanIndex, <thisStereoSubChanIndex;

	var <server, <gui, <curKernel, <stereoActive, <isMuted, <isAttenuated, <stateLoaded, <rotated;
	var <clipMonitoring, <curDecoderPatch, <curDecoderName, <curAmbiFormat, rbtTryCnt, <slSynths;
	var <clipListener, <reloadGUIListener, <clipMonDef, <patcherDef, <sterPatcherDef, <stereoSubPatcherDef, <stereoSubAz;
	var <stereoPatcherSynths, <satPatcherSynths, <subPatcherSynths, <curOrder;
	var <slGroups, synthsLoaded=false;
	var <monitorGroup_ins, <monitorGroup_outs, <monitorSynths_outs, <monitorSynths_ins;
	var <partConvolverPatch, <nextPartConvolverPatch, <stereoGain, <stereoSubGain;
	var <newDecoderPatch, <partConvSynthDef;
	var <fftsize, <configFilePath, <maxOrder, <curRadius, <radiusSizes;

	// SoundLabUtils
	var <compDict, <curDecAttributes, <decAttributes, <decAttributeList, <matrixDecoderNames;
	var <spkrAzims, <spkrElevs, <spkrDirs, <spkrOppDict, <spkrDels, <spkrGains, <spkrDists;
	var <decoderLib, <synthLib, <loadedDelDistGain;
	var <slhw, firstOutput, firstInput;
	var <kernelInputs;
	var <forceCleanupFunc, <recompileWindow;
	var sampleRateChange = false, midiNum, midiVolumeFunc, midiControlSpec;

	*new { |configFileName="CONFIG_205.scd", useKernels=true, loadGUI=true, useSLHW=true|
		^super.newCopyArgs(configFileName, useKernels, loadGUI, useSLHW).init;
	}

	// NOTE: Jack will create numHarwareOuts * 3 for routing to
	// system hardware (0..numHardwareOuts-1)
	// partConvolver 1 (numHardwareOuts..numHardwareOuts*2-1)
	// partConvolver 2 (numHardwareOuts..numHardwareOuts*3-1)
	// Thus SC will boot with s.option.numOutputBusChannels = numHarwareOuts * 3.
	init {

		CtkObj.latency_(nil);

		forceCleanupFunc = {this.cleanup(true)};
		ShutDown.add(forceCleanupFunc);

		if(PathName(configFileName).isAbsolutePath, {
			configFilePath = configFileName;
		}, {
			configFilePath = File.realpath(this.class.filenameSymbol).dirname +/+ configRelativePathName ++ configFileName;
		});

		if(File.exists(configFilePath), {

			File.use(configFilePath, "r", { |f|
				config = f.readAllString.interpret;
			});

		}, {
			Error("File not found at" + configFilePath).throw;
		});


		// defaults
		labName				= config.labName ?? {""};
		numHardwareOuts		= config.numHardwareOuts;
		numHardwareIns		= config.numHardwareIns;
		defaultDecoderName	= config.defaultDecoderName;	// synthDef name
		stereoChanIndex		= config.stereoChanIndex;
		stereoSubChanIndex	= config.stereoSubChanIndex;
		stereoSubAz         = config.stereoSubAz;
		numSatChans			= config.numSatChans;
		numSubChans			= config.numSubChans;
		totalArrayChans		= numSatChans+numSubChans;		// stereo not included
		numKernelChans		= totalArrayChans;				// TODO: confirm this approach
		rotateDegree		= config.rotateDegree ?? {-90};	// default rotation to the right
		stereoGain			= config.stereoGain ?? 0;		// gain in dB to balance stereo with decoders
		stereoSubGain       = config.stereoSubGain ?? 0;
		fftsize             = config.fftsize ?? {1024};
		curAmbiFormat       = 'atk';                       // initialize to FuMa to keep current behaviour, change in the future?
		maxOrder            = config.maxOrder;              // whats the highest decoder for the room?
		curOrder            = maxOrder;
		radiusSizes         = config.radiusSizes;           // a list of radius sizes ['small', 'medium', 'large']
		curRadius           = radiusSizes[0];               // initalize listening radius to first option
		initSR = config.initSampleRate;
		firstInputArr = config.firstInputArr;       //firstInputArr
		firstOutputArr = config.firstOutputArr;
		// Note: shelfFreq in config takes precedence over listeningDiameter
		// order / pi * 340 / listeningDiameter
		// default shelf 400Hz (for dual band decoders)
		shelfFreq			= config.shelfFreq ?? {
			if( config.listeningDiameter.notNil,
				{ 1 / pi * 340 / config.listeningDiameter},
				{400})
		};

		// midi volume control
		midiNum = config.midiNum;

		kernelDirPathName = kernelDirPathName ?? {
			config.kernelsPath !? {
				if(config.kernelsPath[0].asSymbol == '/', {//it's absolute path
					PathName.new(config.kernelsPath);
				}, {
					if(config.kernelsPath[0] == "~", {//it's relative to home directory
						PathName.new(config.kernelsPath.standardizePath);
					}, {//it's relative to the class file
						PathName.new(File.realpath(this.class.filenameSymbol).dirname ++ "/"
							++ config.kernelsPath)
					});
				});
			};
		}; //expecting path relative the class, NOT starting with a slash

		/* Custom matrix decoder TXT files */
		// folder structure under decoderMatricesPath location should be
		// 2 folders: "dual", "single"
		// "dual" contains folders for each set of HF and LF matrix files, the name of the
		// folder becomes the name of the decoder
		// "single" contains the matrix txt files, the name of the file (without .txt)
		// becomes the name of the decoder
		decoderMatricesPath = decoderMatricesPath ?? {
			config.decoderMatricesPath !? {
				if(config.decoderMatricesPath[0].asSymbol == '/', {	//it's absolute path
					PathName.new(config.decoderMatricesPath);
				}, {
					if(config.decoderMatricesPath[0] == "~", {	//it's relative to home directory
						PathName.new(config.decoderMatricesPath.standardizePath);
					}, {//it's relative to the class file
						PathName.new(File.realpath(
							this.class.filenameSymbol).dirname ++ "/"
						++ config.decoderMatricesPath)
					});
				});
			}; //expecting path relative the class, NOT starting with a slash
		};

		globalAmp = 0.dbamp;
		stereoActive = stereoActive ?? {true};
		isMuted = isMuted ?? {false};
		isAttenuated = isAttenuated ?? {false};
		stateLoaded = stateLoaded ?? {false};
		clipMonitoring = clipMonitoring ?? {false};
		rotated = rotated ?? {false};
		matrixDecoderNames = [];

		slGroups = IdentityDictionary.new(know:true);

		this.prInitRigDimensions;
		this.prInitDecoderAttributes;

		rbtTryCnt = 0;

		OSCdef(\clipListener, {
			|msg, time, addr, recvPort|
			("CLIPPED channel" ++ msg[3]).postln;
			this.changed(\clipped, msg[3]);
		}, '/clip', nil
		);

		OSCdef(\reloadGUI, {
			|msg, time, addr, recvPort|
			"reloading gui".postln;
			this.buildGUI;
		}, '/reloadGUI', nil
		);

		OSCdef(\restart, {
			fork {
				this.cleanup;
				0.5.wait;
				thisProcess.recompile;
			}
		}, \restart
		);

		if(usingSLHW, {
			this.prInitSLHW(initSR);
		}, {
			this.prInitDefaultHW(initSR);
		});
	}

	prParseKernelDir {
		kernels = [];
		kernelDirPathName.entries.do({ |sr_pn|
			var sr, nm, knm, result;

			(sr_pn.isFolder && (sr_pn.folderName.asInteger == server.sampleRate)).if{

				sr_pn.entries.do({ |kern_pn|
					kern_pn.isFolder.if{
						// kernel "category name"
						knm = kern_pn.folderName;
						kern_pn.entries.do{ |entry_pn|
							// kernel folder
							if(entry_pn.isFolder, {
								// could add check here for soundfiles within
								// to confirm it's a kernel folder

								// kernel stored as String of the path relative to sample rate
								kernels = kernels.add( knm ++ "/" ++ entry_pn.folderName )
							});
						}
					}
				})
			}
		})
	}

	prCheckKernelSR {
		var thisKernelPath;
		( curKernel.notNil and: (curKernel != \basic_balance) ).if({
			var namedFolder, kernelFolder, pn;

			pn = PathName(curKernel.asString);
			namedFolder = pn.allFolders[pn.allFolders.size-2];
			kernelFolder = pn.allFolders.last;
			("testing " ++ (namedFolder ++ "/" ++ kernelFolder)).postln;

			if( kernels.collect(_.asSymbol).includes((namedFolder ++ "/" ++ kernelFolder).asSymbol), {
				var newPath;
				// update curKernel to new SR path
				newPath = format( "%%/%/%/", config.kernelsPath, server.sampleRate.asInteger, namedFolder, kernelFolder);
				File.exists(newPath).if({
					thisKernelPath = newPath;
				});
			}, {
				this.changed(\reportStatus,
					warn("Last kernel wasn't found at this sample rate. Defaulting to basic_balance.")
				);
			})
		});
		^thisKernelPath
	}

	prDefinePatchSynths {

		sterPatcherDef = CtkSynthDef(\sterpatcher, { arg in_bus=0, amp=1, gain=0;
			var in;
			// stereo patchers don't use ReplaceOut because
			// often stereo channels are shared with
			// satellite channels, and ReplaceOut would overwrite
			// the satellite's bus contents

			// Out.ar(out_bus, In.ar(in_bus, 1))
			var out_bus = \out_bus.kr([0, 1]);
			in = In.ar(in_bus, 2);

			out_bus.do{|this_out, i|
				Out.ar(this_out,
					// catch if stereo channels weren't measured (like 117,
					// where they're speakers apart from the ambisonics rig)
					in[i]
					* (config.spkrGains[stereoChanIndex[i]] ?? 0).dbamp
					* amp
					* gain.dbamp
				)
			};
		});

		stereoSubPatcherDef = CtkSynthDef(\stersubpatcher, { arg in_bus=0, amp=1, gain=0;
			var out_bus = \out_bus.kr((0..stereoSubChanIndex.size-1)).asArray;
			if( stereoSubChanIndex.size == 1,
				{	// summing stereo into 1 sub
					Out.ar(out_bus[0],
						In.ar(in_bus, 2).sum
						* (2.sqrt/2)
						* config.spkrGains[numSatChans].dbamp
						* amp
						* gain.dbamp
					)
				},{ // encoding stereo t b format then decoding to multipl subs
					var stereoBF = FoaEncode.ar(
						In.ar(in_bus, 2),
						FoaEncoderMatrix.newStereo(0) // distortion angle of 0 -> [90, -90]
					);

					// simple cardioid mono decoder for each sub direction
					out_bus.do{|this_out, i|
						Out.ar( this_out,
							FoaDecode.ar(stereoBF,
								FoaDecoderMatrix.newMono(
									stereoSubAz[i],
									0, 0.5)   // no elevation, cardioid
							)
							* (2/stereoSubChanIndex.size) // balance the energy based on the number of subs
							* config.spkrGains[stereoSubChanIndex[i]].dbamp
							* amp
							* gain.dbamp
						)
					};
			});
		});

		clipMonDef = CtkSynthDef(\clipMonitor, { arg in_bus=0, clipThresh = 0.977;
			var sig, peak;
			sig = In.ar(in_bus, 1);
			peak = Peak.ar(sig, Impulse.kr(10));
			SendReply.ar( peak > clipThresh, '/clip', [in_bus, peak] );
		});
	}

	// this happens after hardware is intitialized and server is booted
	prLoadServerSide { |argServer|
		var loadCondition;
		loadCondition = Condition(false);
		"\n*** Loading Server Side ***\n".postln;

		server = argServer ?? {
			"server defaulting because none provided -prLoadServerSide".warn;
			Server.default
		};

		server.doWhenBooted({
			var requestKernel;
			kernelInputs = CtkAudio.new(numKernelChans, server: server);
			if(usingSLHW, {
				server = slhw.server;
				hwInStart = server.options.numOutputBusChannels + slhw.firstInput;
				hwOutStart = slhw.firstOutput;
			}, {
				hwInStart = server.options.numOutputBusChannels;
				hwOutStart = firstOutputArr[0]
			});

			thisStereoChanIndex = stereoChanIndex + hwOutStart;
			thisStereoSubChanIndex = stereoSubChanIndex + hwOutStart;

			// midi Volume control
			// send amp values to a buss that gets read into every synth
			if(midiNum.notNil, {
				MIDIClient.init;
				MIDIIn.connectAll;
				// midi Volume midiFunc
				midiControlSpec = ControlSpec.new(-90, 10, 'db');
				midiVolumeFunc = MIDIFunc.cc({arg ...args;
					var amp;
					amp = midiControlSpec.map(args[0].linlin(0, 127, 0, 1));
					this.amp_(amp)
				}, midiNum); // x-touch one fader number = 70
			});

			// "waiting 2 seconds".postln;
			// 2.wait; // give server time to get sorted

			// get an up-to-date list of the kernels available at this sample rate

			// "kernelDirPathName: ".post; kernelDirPathName.postln;
			kernelDirPathName !? {this.prParseKernelDir};

			kernels = [\basic_balance] ++ kernels;

			// kill any running partConvolvers
			partConvolverPatch !? {"Stopping a running partConvolverPatch".postln; partConvolverPatch.free(xfade)};
			nextPartConvolverPatch !? {"Stopping a running partConvolverPatch".postln; nextPartConvolverPatch.free(xfade)};


			this.prDefinePatchSynths;

			server.sync;

			/* proper NODE TREE order
			monitorGroup_ins
			SoundLabDecoderPatch.group
			>decoder (ambi, thru, etc.)
			>delgainCompSynth
			SoundLabPartConvPathcher.group
			stereoPatcherGroup
			monitorGroup_outs
			*/
			// create all groups

			// monitor in group
			slGroups.monitorGroup_ins = CtkGroup.play(
				addAction: \head,
				target: 0,
				server: server
			);

			// decoder group
			slGroups.decoderPatcherGroup = CtkGroup.play(
				addAction: \head,
				target: 1,
				server: server
			);

			server.sync;

			// part conv group
			slGroups.partConvPatcherGroup = CtkGroup.play(
				addAction: \after,
				target: slGroups.decoderPatcherGroup,
				server: server
			);

			// stereo group
			slGroups.stereoPatcherGroup = CtkGroup.play(
				addAction: \before,
				target: slGroups.partConvPatcherGroup,
				server: server
			);

			server.sync;

			// monitor out group
			slGroups.monitorGroup_outs = CtkGroup.play(
				addAction: \after,
				target: slGroups.partConvPatcherGroup,
				server: server
			);

			server.sync;

			stereoPatcherSynths = [
				// TAIL so avoid sending from stereo into satellite patcher synths (doubling the output)
				sterPatcherDef.note( addAction: \tail, target: slGroups.stereoPatcherGroup )
				.in_bus_(hwInStart)
				.out_bus_(thisStereoChanIndex) // this should be array of channels
				.gain_(stereoGain)
				.play,

				// route stereo to subs as well,
				// this synth also does gain comp on sub(s)
				// add after patcher group to ensure it's the very last synth
				// so it doesn't overwrite any busses
				stereoSubPatcherDef.note( addAction: \tail, target: slGroups.stereoPatcherGroup )
				.in_bus_(hwInStart)
				// TODO: this implies no filter correction on stereo sub send, is that OK?
				.out_bus_(thisStereoSubChanIndex)
				.gain_(stereoGain + stereoSubGain)
				.play
			];
			server.sync;

			// TEST - Maybe we dont need any of this?
			// while({stereoPatcherSynths.collect({|synth|synth.isPlaying}).includes(false)}, {
			// 	"waiting on stereo patchers".postln; 0.02.wait
			// });
			// 0.2.wait; // TODO find a better solution than wait
			// stereoActive.not.if{stereoPatcherSynths.do(_.pause)};

			// CLIP MONITORS
			// - initialized, not played yet
			monitorSynths_ins = numHardwareIns.collect{ |i|
				clipMonDef.note(target: slGroups.monitorGroup_ins)
				.in_bus_(hwInStart + i)
			};
			monitorSynths_outs = (numHardwareOuts*3).collect{ |i|
				// note: in_bus index is a hardware out channel
				clipMonDef.note(target: slGroups.monitorGroup_outs).in_bus_(i)
			};
			server.sync; // to make sure stereo patchers have started

			// in the case of a SR change, check to make sure the curKernel
			// is still available at this sampleRate
			requestKernel = this.prCheckKernelSR;

			requestKernel.isNil.if({this.setNoKernel(false)});

			this.startNewSignalChain(
				if(curDecoderPatch.notNil,
					{curDecoderPatch.decoderName}, // carried over from reboot/sr change
					{defaultDecoderName}
				),
				requestKernel ?? \basic_balance,
				curAmbiFormat,
				curOrder,
				curRadius,
				loadCondition
			);

			loadCondition.wait; "New Signal Chain Loaded".postln;
			loadCondition.test_(false);

			this.stereoRouting_(stereoActive);

			clipMonitoring.if{this.clipMonitor_(true)};
			rotated.if{this.rotate_(true)};
			stateLoaded = true;
			if(loadGUI and: gui.isNil, {this.buildGUI});
			this.changed(\stateLoaded);
			if(midiNum.notNil, {
				this.amp_(-12)
			});

		});
	}

	checkKernelFolder { |pendingKernel|
		var str;
		// build the path to the kernel folder
		str = kernelDirPathName.absolutePath ++ server.sampleRate.asInteger.asString ++ "/" ++ pendingKernel;

		^File.exists(str).if({
			str
		},{
			warn("Folder for this kernel doesn't exist. Check that the folder specified in the config is available at all sample rates.");
			nil; // return
		});
	}

	// kernelPath of nil designates no kernel change
	startNewSignalChain { |decoderName, kernelPath, ambiFormat, newOrder, newRadius, completeCondition|
		var cond = Condition(false), updatedKernel = false;

		fork {
			var testKey;
			"StartNewSignalChain - kernelPath: \t%\n".postf(kernelPath);

			if( kernelPath.notNil, {
				if( kernelPath != \basic_balance, {
					// load partConvolverPatch
					usingKernels = true;
					"loading new partConvolverPatch".postln; // debug
					this.loadPartConv(kernelPath, cond); // this sets nextPartConvolverPatch var
					updatedKernel = true;
				},{
					// setting to basic balance
					this.setNoKernel;
					updatedKernel = true;
					cond.test_(true).signal
				})
			},{
				// no kernel change, just move one
				"no new correction specified".postln;
				updatedKernel = false;
				cond.test_(true).signal
			});

			cond.wait;
			cond.test_(false); // reset the condition to hang when needed later



			// Load delays, distances and gains anew if needed

			if( loadedDelDistGain.isNil					// startup
				or: updatedKernel,     				// kernel change
				{
					var testKey, delDistGainKey;

					delDistGainKey = if( partConvolverPatch.notNil, {
						// build the key from the kernel path and sample rate
						var kpn;
						kpn = PathName(partConvolverPatch.kernelPath);

						testKey = (this.sampleRate.asString ++ "/" ++ kpn.allFolders[kpn.allFolders.size-2]).asSymbol;
					},{
						"Selecting default delay/dist/gain.".postln;
						\default;
					});

					// load delays, distances and gains
					// DEPRECATED for now, handled by HoaDecoderPatch
					// TO-DO, re-add this functionality to HoaDecoderPatch
					// this.prLoadDelDistGain( delDistGainKey, cond );
					// cond.wait;
					// cond.test_(false);

					// if loading delays, distances and gains fails, it will be set to default
					// in which case partConvolverPatch has to be "cancelled"
					if( partConvolverPatch.notNil and: (loadedDelDistGain == \default), {
						partConvolverPatch.free(xfade);
						partConvolverPatch = nil;
						warn( format(
							"partConvolverPatch kernel % doesn't match the key that sets the delays, distances and gains in the decoder synth\n", testKey
						));
					});

					// because delays, distances and gains have changed, need to
					// reload synthdefs
					// NOTE: Now adding synths on the fly with HoaDecoder
					// (synthsLoaded.not or: sampleRateChange).if({
					// 	"\n*** Loading SynthDefs ***\n".postln;
					// 	this.prLoadSynthDefs(cond);
					// 	cond.wait;
					// 	cond.test_(false);
					// 	"\n*** SynthDefs loaded ***\n".postln;
					// });


			});

			server.sync; // sync to let all the synths load


			// start new decoder if needed
			if(decoderName.notNil or: (curAmbiFormat != ambiFormat) or: (curOrder != newOrder) or: (curRadius != newRadius),			// requested decoder change
				{
					if((decoderName != curDecoderName) or: sampleRateChange or: (curAmbiFormat != ambiFormat) or: (curOrder != newOrder) or: (curRadius != newRadius), {
						var newDecName;
						// if no decoderName given, create new decoder matching the current one
						newDecName = decoderName ?? {
							curDecoderPatch !? {curDecoderPatch.decoderName}
						};

						if( newDecName.notNil, {
							// TODO: is this the best behaviour? This is most like what it was before, i.e. using
							// a decoder assumes you are sending the expected format
							// user will have to specify a mismatch of formats???
							postf("New decoder starting: %\n", newDecName);
							this.startDecoder(newDecName, ambiFormat, newOrder, newRadius, cond);
							cond.wait;
							cond.test_(true).signal;
							this.prUpdatePausedPartConv;

						},{
							warn( "No decoder name provided and no current decoder name found -
NO NEW DECODER STARTED");
							cond.test_(true).signal;
						})
					}, {
						("Decoder " ++ curDecoderName ++ " is already running.").postln;
						cond.test_(true).signal;
					})

				}, {
					warn("NO NEW DECODER CREATED - no nextPartConvolverPatch and/or no decoder name provided!");
					cond.test_(true).signal;

			});


			cond.wait;

			sampleRateChange = false;

			"\n*** END ***\n".postln;
			completeCondition !? {completeCondition.test_(true).signal};
		}
	}

	// pause and unpause appropriate partconv channel synths
	prUpdatePausedPartConv {
		var satOutbusNums, remainder;
		partConvolverPatch !? {
			curDecoderPatch.decoder !? {
				satOutbusNums = curDecoderPatch.decoder.satOuts;
				// unpause
				remainder = (0..numSatChans-1).select({ |index|
					satOutbusNums.includes(index).not
				});
				partConvolverPatch.pause(remainder);
				partConvolverPatch.unPause(satOutbusNums)
			}
		}
	}

	// cleanup server objects to be reloaded after reboot
	prClearServerSide { |finishCondition|
		var cond = Condition.new;
		fork {
			curDecoderPatch.free(xfade, cond);
			// curDecoderPatch = nil; // removed so variable remains for reload/sr change
			cond.wait;
			cond.test_(false);
			partConvolverPatch !? {
				partConvolverPatch.free(xfade, cond);
				cond.wait;
				cond.test_(false)
			};
			nextPartConvolverPatch !? {
				nextPartConvolverPatch.free(xfade, cond);
				cond.wait;
				cond.test_(false)
			}; // ...just in case
			slGroups.do(_.free);
			stateLoaded = false;
			finishCondition !? {finishCondition.test_(true).signal}
		}
	}

	startDecoder  { |newDecName, ambiFormat, newOrder, newRadius, completeCondition, hoaPsyFoclSpecs|
		var cond, decoutbus, decSpecs, orderWarning = false, why, updateDec, oldDiscrete = false;
		cond = Condition(false);
		{
			decSpecs = decAttributes[newDecName.asString];
			// select which of the 3 out groups to send decoder/correction to
			decoutbus = usingKernels.if({ kernelInputs.bus },{ hwOutStart });	// default for no kernels
			curDecoderPatch.notNil.if({
				oldDiscrete = curDecoderPatch.decoder.decoderType == 'discrete'
			});

			(newOrder.isNil and: curOrder.isNil).if({ curOrder = decSpecs.maxOrder });

			// if we have a new decoder, post to GUI
			updateDec = newDecName != curDecoderName;

			newDecoderPatch = HoaDecoderPatch(
				configFilePath,
				newDecName,
				newOrder ?? { curOrder },
				1,
				ambiFormat ?? { curAmbiFormat },
				newRadius ?? { curRadius },
				server
			);

			// SoundLabDecoderPatch(this,
			// 	decoderName: newDecName,
			// 	decSpecs: hoaPsyFoclSpecs ?? {config.hoaPsyFoclSpecs}, //  for hoa only
			// 	inbusnum: hwInStart, 	// decoder inbusnum
			// 	outbusnum: decoutbus,	// decoder outbusnum
			// 	group: slGroups.decoderPatcherGroup,
			// 	stereoActive: stereoActive,
			// 	loadCondition: cond	    // finishCondition
			// );
			//
			// cond.wait;
			// cond.test_(false);

			// if initializing SoundLabDecoderPatch fails, decoderName won't be set
			newDecoderPatch.decoderName !? {

				postf("newDecoderPatch initialized, playing: % \n", newDecoderPatch.decoderName); // debug
				curDecoderPatch !? {
					curDecoderPatch.free(xfade, cond);
					cond.wait
				};
				newDecoderPatch.play(hwInStart, decoutbus, xfade: xfade, target: slGroups.decoderPatcherGroup, stereo: stereoActive, amp: globalAmp);
				curDecoderPatch = newDecoderPatch;
				curDecoderName = curDecoderPatch.decoderName;

				// tell the GUI what has changed
				updateDec.if({
					this.changed(\decoder)
				});
				(curDecoderPatch.decoder.decoderType != 'discrete').if({
					decSpecs.maxOrder.notNil.if({
						orderWarning = (newOrder ?? { curOrder }) > decSpecs.maxOrder;
						orderWarning.if({ why = newDecName })
					});
					((curDecoderPatch.decoder.inputFormat == 'fuma') and: (newOrder ?? { curOrder } > 1)).if({ orderWarning = true; why = "fuma" });
					oldDiscrete.if({
						this.changed(\order, orderWarning, why, updateDec);
						curOrder = curDecoderPatch.decoder.satOrder;
						curAmbiFormat = curDecoderPatch.decoder.inputFormat;
						this.changed(\format);
						curRadius = curDecoderPatch.decoder.listeningRadius;
						this.changed(\radius)
					}, {

						(curDecoderPatch.decoder.satOrder != curOrder).if({
							this.changed(\order, orderWarning, why, updateDec);
							curOrder = curDecoderPatch.decoder.satOrder;
						});
						(curAmbiFormat != curDecoderPatch.decoder.inputFormat).if({
							curAmbiFormat = curDecoderPatch.decoder.inputFormat;
							this.changed(\format, curAmbiFormat)
						});
						(curRadius != curDecoderPatch.decoder.listeningRadius).if({
							curRadius = curDecoderPatch.decoder.listeningRadius;
							this.changed(\radius)
						})
					})

				})

			};

			completeCondition !? { completeCondition.test_(true).signal };
		}.forkIfNeeded
	}

	loadPartConv { |newKernelPath, completeCondition, timeout = 5|
		var kernelDir_pn, partSize, k_size,
		numFoundKernels = 0, numtries = 50, trycnt=0,
		scOutbusConnect, partConvHWOut, cond;
		cond = Condition(false);
		fork {
			block { |break|
				kernelDir_pn = PathName(newKernelPath); //this.prFindKernelDir(newKernel);
				kernelDir_pn.postln;
				kernelDir_pn ?? {
					this.changed(\reportStatus, warn("Kernel name not found: "++newKernelPath++".  No longer using kernels!"));
					partConvolverPatch ?? {
						// if no kernel already loaded, not using kernels
						warn("No longer usingKernels");
						this.setNoKernel;
					};
					break.();
				};

				// initialize partConvolver variables
				kernelDir_pn.filesDo({ |file|
					if(file.extension == "wav", {
						numFoundKernels = numFoundKernels + 1;
					})
				});
				// debug
				postf("path to kernels: % \npartition size: % \n",
					newKernelPath, fftsize
				);
				// check that we have enough kernels to match all necessary speakers
				if( numFoundKernels != numKernelChans, {
					var msg;
					msg = format("Number of kernels found (%) does not match the numKernelChannels (%)!", numFoundKernels, numKernelChans);
					warn(msg);
					this.changed(\reportStatus, msg);
					break.();
				});

				// set part conv size dependent on SR
				partSize = fftsize[(server.sampleRate > 48000).asInteger];

				// scale up part conv size for HOA
				// TO-DO: do we still need this??
				// curDecAttributes.kind.asString.contains("hoa").if({ partSize = partSize * 2 });

				"Starting PartConvPatch".postln;
				nextPartConvolverPatch = SoundLabPartConvPatch.new(this,
					kernelPath: newKernelPath,
					fftsize: partSize,
					inbusnum: kernelInputs,
					outbusnum: hwOutStart,
					group: slGroups.partConvPatcherGroup,
					loadCondition: cond
				);

				cond.wait;

				cond.test_(false);

				"Playing convolution synths!".postln;

				nextPartConvolverPatch.play(xfade);

				"Updating decoder for new kernels".postln;
				curDecoderPatch.outbusnum_(kernelInputs.bus);
				// curDecoderPatch.kernelUpdate;

				"Updating Stereo Routing".postln;
				this.stereoRouteForKernel(true);

				partConvolverPatch !? { partConvolverPatch.free(xfade) };

				partConvolverPatch = nextPartConvolverPatch;		// update var with new instance
				curKernel = partConvolverPatch.kernelPath;
				nextPartConvolverPatch = nil;				// reset var
				this.changed(\kernel, curKernel);

				this.prUpdatePausedPartConv

			};
			completeCondition !? {completeCondition.test_(true).signal};
		}
	}

	buildGUI {
		if (gui.isNil) {
			gui = SoundLabGUI.new(this);
		} {
			fork {
				gui.cleanup;
				1.wait;
				gui = SoundLabGUI.new(this);
			}
		}
	}

	// ------------------------------------------------------------------
	// -------- State Setters/Getters -----------------------------------
	// ------------------------------------------------------------------

	stereoRouting_ { |bool|
		stereoActive = bool;
		if( stereoActive,
			{
				curDecoderPatch !? {
					curDecoderPatch.routeForStereo(stereoActive);
				};
				stereoPatcherSynths.do(_.run);
				// Per converstaion with marcin, simply move the inputs instead of pausing?
				// stereoPatcherSynths.do(_.in_bus_(hwInStart))
			}, {
				curDecoderPatch.notNil.if({
					curDecoderPatch.routeForStereo(stereoActive);
					stereoPatcherSynths.do(_.pause);
					// Per converstaion with marcin, simply move the inputs instead of pausing?
					// stereoPatcherSynths.do(_.in_bus_(hwInStart + curDecAttributes.numInputChans))
				}, {
					"No Decoder! Leaving Stereo Alone!".postln
				})

		});

		this.changed(\stereo, stereoActive)

	}

	stereoRouteForKernel { |bool|
		if( bool,
			{
				[stereoChanIndex, stereoSubChanIndex].do{ |thisIndex, i|
					stereoPatcherSynths[i].out_bus_(thisIndex + kernelInputs.bus)
				}
			}, {
				[thisStereoChanIndex, thisStereoSubChanIndex].do{ |thisIndex, i|
					stereoPatcherSynths[i].out_bus_(thisIndex)
				}
			}
		)
	}

	mute { |bool = true|
		if(bool,
			{
				curDecoderPatch.decodersynth.amp_(0);
				isMuted = true;
				this.changed(\mute, 1);
			},{
				isMuted = false; // must set before for this.attenuate isMuted check to work
				this.changed(\mute, 0);
				if( isAttenuated,
					{this.attenuate},
					{curDecoderPatch.decodersynth.amp_(globalAmp)}
				);
			}
		)
	}

	attenuate { | bool = true, att_dB = -30|
		if(bool,
			{
				if( isMuted.not )
				{curDecoderPatch.decodersynth.amp_(att_dB.dbamp)};

				isAttenuated = true;
				this.changed(\attenuate, 1);
			},{
				if( isMuted.not )
				{curDecoderPatch.decodersynth.masteramp_(globalAmp)};

				isAttenuated = false;
				this.changed(\attenuate, 0);
			}
		)
	}

	rotate_ { |bool|
		block({ |break|
			(rotated == bool).if{break.("rotation setting already current".warn)};
			(curDecoderPatch.decoder.decoderType == \discrete).if{
				this.changed(\reportStatus, "routing is discrete, no rotation");
				break.("routing is discrete, no rotation".warn);
			};
			if( bool,
				{
					curDecoderPatch.decodersynth.rotate_(rotateDegree.degrad);
					rotated = true;
				},{
					curDecoderPatch.decodersynth.rotate_(0);
					rotated = false;
			});
			this.changed(\rotate, bool);
		});
	}

	amp_ { |amp_dB|
		var ampnorm;
		ampnorm = amp_dB.dbamp;
		// only update amp if not muted or att
		if( isAttenuated.not && isMuted.not, {
			curDecoderPatch.decodersynth.amp_(ampnorm); // set decoder amp
			stereoPatcherSynths.do(_.amp_(ampnorm)); // set stereo channels amp (including subs)
		});
		globalAmp = ampnorm; // normalized, not dB
		this.changed(\amp, globalAmp);
	}

	sweetSpotDiam_ { |diam, order = 1| // diam in meters
		var freq;
		freq = order / pi * 340 / diam;
		curDecoderPatch !? { curDecoderPatch.decodersynth.shelfFreq_(freq) };
		shelfFreq = freq;
		this.changed(\shelfFreq, freq);
		this.changed(\sweetSpotDiam, diam);
	}

	getDiamByFreq { |freq, order = 1|
		^ order / pi * 340 / freq;
	}

	clipMonitor_{ | bool = true |
		if( bool,
			{(monitorSynths_ins ++ monitorSynths_outs).do(_.play); clipMonitoring = true;},
			{(monitorSynths_ins ++ monitorSynths_outs).do(_.free); clipMonitoring = false;}
		);
	}

	sampleRate_ { |newSR|
		var cond;
		cond = Condition(false);
		this.prClearServerSide(cond);
		sampleRateChange = true;
		cond.wait;
		if(usingSLHW,
			{slhw.startAudio(newSR)},
			{this.changed(\stoppingAudio); this.prInitDefaultHW(newSR); slhw !? { server = slhw.server }}
		)
	}

	sampleRate {^server.sampleRate.asInteger}

	setNoKernel { |statusChange = true|
		curKernel = \basic_balance;
		usingKernels = false;
		statusChange.if({ this.changed(\kernel, curKernel) });
		partConvolverPatch !? {
			partConvolverPatch.free(xfade);
			partConvolverPatch = nil;
		};
		curDecoderPatch !? { curDecoderPatch.outbusnum_(hwOutStart) };
		this.stereoRouteForKernel(false)
	}

	// responding to changes in SoundLabHardware
	update {
		| who, what ... args |
		if( who == slhw, {	// we're only paying attention to one thing, but just in case we check to see what it is
			switch ( what,
				\audioIsRunning, {
					switch(args[0],
						true, {
							server = slhw.server ?? {warn("loading default server"); Server.default;};
							if(stateLoaded.not, {this.prLoadServerSide(server)})
						},
						false, { "Audio stopped running in hardware.".postln }
					)
				},
				\stoppingAudio, {
					this.prClearServerSide;
				},
			)
		})
	}


	prInitRigDimensions {
		// TODO consider adding sat and sub keys to compDict
		compDict = IdentityDictionary.new(know: true).putPairs([
			\gains, IdentityDictionary.new(know: true),
			\distances, IdentityDictionary.new(know: true),
			\delays, IdentityDictionary.new(know: true)
		]);

		/* distances (m): */
		compDict.distances.put( \default, config.spkrDists );

		/* calculate delays (sec): */
		compDict.delays.put( \default,
			config.spkrDels ??
			// calculate from distance
			(compDict.distances.default.maxItem - compDict.distances.default) / AtkHoa.speedOfSound;
		);

		/* gains (dB) */
		compDict.gains.put( \default, config.spkrGains );

		/* parse distance delay and gain files from kernel folders */
		// Folder structure: /sampleRate/kernelName/ holds
		// delay, dist, gain .txt files and folders for each "version"
		// of the kernel, i.e. the various settings: moderate, high correction, min/lin phase, etc
		kernelDirPathName !? {
			kernelDirPathName.entries.do({ |sr_pn|
				var sr, nm, knm, result;

				(sr_pn.isFolder && (sr_pn.folderName.asInteger !=0)).if{
					sr = sr_pn.folderName;
					sr_pn.entries.do({ |kern_pn|
						kern_pn.isFolder.if{
							knm = kern_pn.folderName;
							kern_pn.entries.do{ |file_pn|
								if(file_pn.isFile, {
									nm = file_pn.fileName;
									case
									{nm.contains("delays")}{
										debug.if{postf("\nParsing delays for %, %: ", knm, sr)};
										compDict.delays.put(
											(sr++"/"++knm).asSymbol, this.prParseFile(file_pn)
										);
									}{nm.contains("distances")}{
										debug.if{postf("\nParsing distances for %, %:", knm, sr)};
										compDict.distances.put(
											(sr++"/"++knm).asSymbol, this.prParseFile(file_pn)
										);
									}{nm.contains("gains")}{
										debug.if{postf("\nParsing gains for %, %:", knm, sr)};
										compDict.gains.put(
											(sr++"/"++knm).asSymbol, this.prParseFile(file_pn)
										);
									};
								});
							}
						}
					})
				}
			});
		};

		/* azimuth angles */
		spkrAzims = config.spkrAzimuthsRad;
		/* elevation angles */
		spkrElevs = config.spkrElevationsRad;
		// pack azims and elevs into directions [[az0, el0],[az1, el1],..]
		spkrDirs = [ spkrAzims, spkrElevs ].lace( spkrAzims.size + spkrElevs.size ).clump(2);
		// this is for the diametric decoder of satellites, so drop the subs
		// spkrDirs = spkrDirs.keep(numSatChans);
		// for diametric decoders
		spkrOppDict = config.spkrOppDict;

		"\n**** rig coordinates initialized ****".postln;
	}

	prInitDecoderAttributes {
		decAttributeList = PathName(PathName(configFilePath).parentPath +/+ config.decoderFolder).files;
		// build an Array from the above attributes
		decAttributes = Dictionary.new;
		decAttributeList.do({ |pathName|
			decAttributes.put(pathName.fileNameWithoutExtension, pathName.fullPath.load)
		});

		"\n**** decoder attributes initialized **** ".postln;
	}

	/*	load speaker delays, distances, gains here because
	in the case of using kernels, it can be samplerate
	dependent, and so needs to happen after server has
	been initialized.
	*/
	prLoadDelDistGain { |delDistGainKey, completeCondition|
		fork {
			var key;

			// test that the kernel key returns a result
			key = if( usingKernels, {

				("trying del dist gains key: "++delDistGainKey).postln; // debug

				if( compDict.distances.includesKey(delDistGainKey) and:
					compDict.delays.includesKey(delDistGainKey) and:
					compDict.gains.includesKey(delDistGainKey),
					{
						delDistGainKey
					},{
						warn(format("Did not find a matching value in the compDict for the key %\nLoading default delays, distances and gains.\n", delDistGainKey));

						\default;
					}
				);
			},{ \default });

			spkrDists =	compDict.distances.at(key);
			spkrDels =	compDict.delays.at(key);
			spkrGains = compDict.gains.at(key);

			this.prCheckArrayData;

			postf("\n*** Delays, gains, distances loaded for:\t% ***\n", key);
			loadedDelDistGain = key;
			completeCondition !? {completeCondition.test_(true).signal};
		}
	}

	checkKernelSpecAtSR { |relativePath|
		var result;
		result = File.exists(config.kernelsPath ++ this.sampleRate ++ "/" ++ relativePath);
		result.not.if{ warn(format("kernel spec entry % not found at this sample rate (%)", relativePath, this.sampleRate)) };
		^result
	}

	collectKernelCheckBoxAttributes {
		var attributes, sRate_pn;
		attributes = [];
		config.kernelsPath !? {
			sRate_pn = PathName( config.kernelsPath ++ this.sampleRate);
			config.kernelSpec.do{|k_attributes|

				// check that the kernel spec exists at this SR
				if( this.checkKernelSpecAtSR(k_attributes[0]), {

					// drop the kernel path and correction degree leaving only user-defined attributes
					k_attributes[1].do{ |att|
						if( attributes.includes(att).not, {attributes = attributes.add(att)} )
					};
				})
			};
		};
		^attributes
	}

	collectKernelPopUpAttributes {
		var popups;
		popups = [[]];

		config.kernelsPath !? {
			config.kernelSpec.do{|k_attributes|

				if( this.checkKernelSpecAtSR(k_attributes[0]), {
					var numPopUps = k_attributes[2].size;

					// grow popups array if needed
					if( popups.size < numPopUps, {
						(numPopUps - popups.size).do{popups = popups.add([])}
					});

					k_attributes[2].do{ |att, i|

						if( popups[i].includes(att).not, {
							popups[i] = popups[i].add(att);
						} )
					};
				});
			};
		};
		^popups
	}

	getKernelAttributesMatch { |selectedAttributes|
		var results, numMatches;
		// gather bools for each kernel spec whether all selected
		// attributes match, should only be one match
		results = config.kernelSpec.collect{ |k_attributes|
			var collAttributes, test1, test2;

			// collect menu and check box attributes for this kernel spec
			collAttributes = (k_attributes[1] ++ k_attributes[2]);

			// return true if all attributes match, false if not
			test1 = selectedAttributes.collect({ |att|
				collAttributes.includes(att)
			}).includes(false).not;
			test2 = collAttributes.collect({ |att|
				selectedAttributes.includes(att)
			}).includes(false).not;

			(test1 and: test2)
		};

		// postf("selectedAttributes:%\n", selectedAttributes);
		// postf("kernel matching results:\n%\n", results);

		numMatches = results.occurrencesOf(true);

		^case
		{numMatches == 1} { config.kernelSpec[results.indexOf(true)][0] }
		{numMatches == 0} { 0 }		// return 0 for no matches
		{numMatches > 1 } { -1 };	// return -1 for more than one match
	}

	formatKernelStatePost { |kPath, short=false|
		var rtn;
		^if( kPath != \basic_balance,
			{ var pn, category, attributes;
				pn = PathName(kPath.asString);
				category = pn.allFolders[pn.allFolders.size-2];
				attributes = config.kernelSpec.select({ |me|
					me.at(0) == (category ++ "/" ++ pn.allFolders.last ++ "/")

				}).at(0).drop(1).flat;

				short.if(
					{ format("%", attributes)},
					{ format("%\n%", category, attributes)}
				);
			},{
				\basic_balance.asString
			}
		);
	}

	prInitSLHW { |initSR|
		slhw = SoundLabHardware.new(
			false, 						//useSupernova
			config.useFireface,			//useFireface
			config.midiDeviceName,		//midiDeviceName
			config.midiPortName,		//midiPortName
			numHardwareIns, 			//serverIns
			numHardwareOuts,    		//serverOuts
			config.audioDeviceName,     //audioDeviceName
			firstInputArr,              //firstInputArr
			firstOutputArr,             //firstOutputArr
			config.hwBufferSize,        //hardwareBufferSize
		);
		// slhw = SoundLabHardware.new(false,true,false,nil,nil,"/usr/local/bin/jackdmp",32,128); //for osx
		slhw.postln;
		slhw.startAudio(
			initSR  				//newSR
		);
		slhw.addDependant(this);
	}

	prInitDefaultHW { |initSR|
		var so;
		// debug
		"initializing default hardware".postln;

		server = server ?? Server.default;
		server !? { server.serverRunning.if{ server.quit} };
		"REBOOTING".postln;

		so = server.options;
		so.sampleRate = initSR ?? 48000;
		so.memSize = 8192 * 16;
		so.numWireBufs = 64*8;
		// so.device = "JackRouter";
		// numHardwareOuts*3 to allow fading between settings,
		// routed to different JACK busses
		so.numOutputBusChannels = numHardwareOuts;
		so.numInputBusChannels = numHardwareIns;

		// the following will otherwise be called from update: \audioIsRunning
		server.waitForBoot({
			rbtTryCnt = rbtTryCnt+1;
			// in case sample rate isn't set correctly the first time (SC bug)
			if( server.sampleRate.asInteger == initSR, {
				rbtTryCnt = 0;
				this.prLoadServerSide(server);
			},{ fork{
				1.5.wait;
				"reboot sample rate doesn't match requested, retrying...".postln;
				if(rbtTryCnt < 3,
					{ this.prInitDefaultHW(initSR) }, // call self
					{ this.changed(\reportStatus, "Error trying to change the sample rate after 3 tries!".warn)}
				)
			}}
			)
		});
	}

	prFindKernelDir { |kernelName|
		var kernelDir_pn;
		kernelDirPathName.folders.do({ |sr_pn|
			if( sr_pn.folderName.asInteger == server.sampleRate.asInteger, {
				sr_pn.folders.do({ |kernel_pn|
					if( kernel_pn.folderName.asSymbol == kernelName, {
						("found kernel match"+kernel_pn).postln;
						kernelDir_pn = kernel_pn; });
				});
			})
		});
		^kernelDir_pn
	}

	// parse text file for delays, distances, gains
	// expects individual .txt files for each
	// with \n -separated float values
	prParseFile { |pathname|
		var data;
		data = [];
		File.use(pathname.fullPath, "r", { |f|
			var str, splt;
			str = f.contents;
			// divide file by newlines
			splt = str.split($\n );
			splt.do({|val, i|
				// filter out spurious newlines at the end
				if( val.contains("."), { // floats will have decimal
					// debug.if{postf("%, %; ", i, val.asFloat)};
					debug.if{postf("% ", i)};
					data = data.add(val.asFloat);
				})
			});
			^data;
		});
	}

	prCheckArrayData {
		postf(
			"Checking array data...\nThese should equal % (numSatChans + numSubChans)\n[%, %, %, %, %, %]\n",
			numSatChans+numSubChans, spkrAzims.size, spkrElevs.size, spkrDists.size,
			spkrDels.size, spkrGains.size, spkrDirs.size
		);

		if (
			spkrAzims.size == spkrElevs.size and:
			spkrElevs.size == spkrDists.size and:
			spkrDists.size == spkrDels.size and:
			spkrDels.size == spkrGains.size and:
			spkrGains.size == totalArrayChans,
			{ "OK: Array sizes of rig dimensions match!".postln },
			{ "Mismatch in rig dimension array sizes!".warn }
		);

		"\n**** Speaker Gains, Distances, Delays ****".postln;
		"Chan: Gain Distance Delay".postln;
		(numSatChans+numSubChans).do({ |i|
			postf("%:\t%\t%\t%\n",
				i, spkrGains[i], spkrDists[i], spkrDels[i]
			)
		});

		"\n**** Speaker Directions ****".postln;
		"Chan: Azimuth Elevation".postln;
		(numSatChans+numSubChans).do({ |i|
			postf("%:\t %\n", i, spkrDirs[i].raddeg)
		});
	}

	createRecompileWindow {|bounds|
		recompileWindow = Window.new(format("% - recompile window", this.class.name), bounds).front;
		recompileWindow.layout_(VLayout(
			nil,
			Button()
			.states_([
				["Recompile class library", Color.red(1, 0.7), Color.grey(0, 0.1)],
			])
			.font_(Font(size: 32))
			.action_({
				recompileWindow.close;
				{thisProcess.recompile}.defer(0.1); //wait for the window to close before recompiling
			}),
			nil
		));
	}


	//force = true will make the cleanup run immediately
	//that means no sound fadeout
	//but can be run from library shutdown

	cleanup  {|force = false|
		if(force, {format("%: starting immediate cleanup", this.class.name).warn});
		ShutDown.remove(forceCleanupFunc);
		[OSCdef(\clipListener), OSCdef(\reloadGUI)].do(_.free);
		gui !? {gui.cleanup};
		slhw !? {slhw.removeDependant(this)};
		this.prClearServerSide; 			// frees jconvs
		slhw !? {slhw.stopAudio(force)};
		recompileWindow !? {{recompileWindow.close}.defer};
		kernelInputs.free;
		midiVolumeFunc.free
	}

	free {|force = false| this.cleanup(force)}
}

/*
------ TESTING ---------
(
s.options.numOutputBusChannels_(32);
s.options.device_("JackRouter");
s.options.numWireBufs_(64*8);
Jconvolver.jackScOutNameDefault = "scsynth:out";
Jconvolver.executablePath_("/usr/local/bin/jconvolver");
// make sure Jack has at least [3x the number of your hardware output busses] virtual ins and outs
// if using the convolution system and you intend to switch between kernel sets
// and default (no convolution) settings

// InterfaceJS.nodePath = "/usr/local/bin/node";

//configFileName="CONFIG_205.scd", useKernels=true, loadGUI=true, useSLHW=true
// l = SoundLab(configFileName:"CONFIG_TEST_205.scd", useKernels:false, loadGUI:true, useSLHW:false)
// l = SoundLab(configFileName:"CONFIG_TEST_117.scd", useKernels:false, loadGUI:true, useSLHW:false)
l = SoundLab(configFileName:"CONFIG_TEST_117.scd", useKernels:true, loadGUI:true, useSLHW:false)
// l = SoundLab(configFileName:"CONFIG_TEST_117.scd", useKernels:false, loadGUI: false, useSLHW:false)
// l = SoundLab(configFileName:"CONFIG_TEST_113.scd", useKernels:false, loadGUI:true, useSLHW:false)
)

l.buildGUI
l.gui = nil
l.gui.cleanup

Open in browser:
http://localhost:8080/

l.rotated
l.rotateDegree

l.startNewSignalChain(\Dome_15ch)
l.startNewSignalChain(\Dome_9ch)
l.startNewSignalChain(\Dome_11ch)

s.scope(2)
"~~~~~~".postln
l.decoderLib.dict.keys

l.free
s.quit

// test signal
s.meter
// test b-format
x = {Out.ar(l.curDecoderPatch.inbusnum, 4.collect{PinkNoise.ar * SinOsc.kr(rrand(3.0, 5.0).reciprocal).range(0.0, 0.15)})}.play
// test discrete
x = {Out.ar(l.curDecoderPatch.inbusnum, 5.collect{PinkNoise.ar * SinOsc.kr(rrand(3.0, 5.0).reciprocal).range(0.0, 0.15)})}.play
x.free
s.scope
s.meter

// testing decoder/kernel switching
l.decoderLib.synthdefs.do{|sd|sd.name.postln}
l.startNewSignalChain(\Sphere_24ch)
l.startNewSignalChain(\Sphere_12ch)
l.startNewSignalChain(\Sphere_12ch, kernelName: \decor)
l.startNewSignalChain(\Sphere_24ch, kernelName: \decor_700)

// testing sample rate change
l = SoundLab(useKernels:false, useSLHW:false)
l.startNewSignalChain(\Sphere_12ch_first_dual)
l.startNewSignalChain(\Dodec)
l.startNewSignalChain(\Quad_Long)
l.startNewSignalChain(\Hex)
l.startNewSignalChain(\Thru_All, \NA)
l.decoderLib.dict.keys

l.sampleRate_(44100)
l.sampleRate_(96000)


// testing slhw
l = SoundLab(useSLHW:false, useKernels:false)

// testing gui
l = SoundLab(48000, loadGUI:true, useSLHW:false, useKernels:false)

x = {Out.ar(0, 4.collect{PinkNoise.ar * SinOsc.kr(rrand(3.0, 5.0).reciprocal).range(0, 0.35)})}.play


InterfaceJS.nodePath = "/usr/local/bin/node"
l = SoundLab(useSLHW:false)
l.cleanup
l.jconvolver
l.gui
l.jconvolver.free
l.gui.buildControls
l.kernelDict

q = SoundLabHardware.new(useFireface:false,midiPortName:nil,cardNameIncludes:nil,jackPath:"/usr/local/bin/jackdmp");
q.dump
q.startAudio(periodNum: 1)
q.stopAudio
s.scope(2,3)
"/usr/local/bin/jackdmp -R  -dcoreaudio -r96000 -p256 -n1 -D".unixCmd
*/