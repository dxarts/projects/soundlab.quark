SoundLabPartConvPatch {
	// copyArgs
	var <sl, <kernelPath, <fftsize, <inbusnum, <outbusnum, <group, <loadCondition;
	var <server, <group, <partConvSynths, <irbuffers, bufSizes, irspectrums, <partConvSynthDef;
	var cond, inbusnumber;

	*new { |soundlab, kernelPath, fftsize, inbusnum, outbusnum, group, loadCondition|
		^super.newCopyArgs(soundlab, kernelPath, fftsize, inbusnum, outbusnum, group, loadCondition).init;
	}

	init {
		fork {
			block { |break|
				cond = Condition.new;
				irbuffers = [];

				inbusnumber = inbusnum.bus;

				server = sl.server;

				"Loading Kernels".postln;

				PathName(kernelPath).deepFiles.do{ |pathname|
					(pathname.extension == "wav").if({
						irbuffers = irbuffers.add(CtkBuffer(pathname.fullPath, server: server).load(sync: false))
					})
				};

				server.sync;

				bufSizes = irbuffers.collect{ |irbuffer|
					PartConv.calcBufSize(fftsize, irbuffer)
				};

				irspectrums = bufSizes.collect{ |bufSize|
					CtkBuffer.buffer(bufSize, 1, server).load(sync: false)
				};

				server.sync;

				irspectrums.do{ |irspectrum, k|
					irspectrum.preparePartConv(0.0, irbuffers[k], fftsize);
					// ("loading part conv buffer:" + k).postln;
					// cond.wait;
					// cond.test_(false)
				};

				server.sync;


				partConvSynthDef = CtkSynthDef('soundLabPartConv', {
					arg buffer, in_busnum, out_busnum, gate = 1, fadeTime;
					var in, conv, env;
					env = EnvGen.ar(
						Env( [0,1,0],[fadeTime, fadeTime],\sin, 1),
						gate, doneAction: 2
					);

					in = In.ar(in_busnum, 1);

					conv = PartConv.ar(in, fftsize, buffer);

					Out.ar(out_busnum, conv * env)
				});

				server.sync;

				"Loaded partconv synth".postln;

				partConvSynths = irspectrums.collect{ |buffer, i|
					partConvSynthDef.note(
						addAction: \head, target: group)
					.buffer_(buffer)
					.in_busnum_(inbusnumber + i)
					.out_busnum_(outbusnum + i);
				};

				"loaded partconv notes".postln;

			};
			loadCondition.test_(true).signal;
		}
	}

	play { |xfade = 0.2|
		partConvSynths.do{ |partConvSynth|
			partConvSynth.fadeTime_(xfade).play
		}
	}

	pause { |indices|
		indices.do{ |index|
			partConvSynths[index].isPaused.not.if({
				(index.asString + "is paused").postln;
				partConvSynths[index].pause
			})
		}
	}

	unPause { |indices|
		indices.do{ |index|
			partConvSynths[index].isPaused.if({
				(index.asString + "is unpaused").postln;
				partConvSynths[index].run
			})
		}

	}

	free { |xfade = 0.2, cond|
		fork {
			partConvSynths.do{ |partConvSynth|
				partConvSynth.fadeTime_(xfade).release
			};
			xfade.wait;
			cond !? { cond.test_(true).signal }
		}
	}

}