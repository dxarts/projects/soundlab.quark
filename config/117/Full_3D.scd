// main parameters
IdentityDictionary(know: true)
.putPairs([
	'numSatChans',			24, // the number of satellite speakers
	'numSubChans', 			1, //  the number of sub-woofers, if no subs set to 0
	'outbusNums',           (0..24), //  the actual output channel numbers for each speaker (including subs at the end)
	'decoderType',          'modeMatch', // decoder type ('modeMatch', 'panto', 'matrix')
	'beamShape',            'basic', // beam shape of the decoder
	'match',                'amp',
	'maxOrder',             3, // highest order this decoder can handle
])
.put('focl',  // remove from Dict if not using Focalization Kernels
	IdentityDictionary.new(know:true)
	.putPairs([
		'psyFoclSize',   1024,
		'alpha',         1.0,
		'foclRadius',    AtkHoa.refRadius,
		'satDim',        3, // 3D for sats
		'match',         'fenergy',
		'sampleRate',    nil, //  set to nil or ommit to use the server sample rate
		'psyFoclKernelPath',     Atk.userExtensionsDir +/+ "foclKernels/" // where do you want to write the kernels (folder will be created if necessary)
	])
)