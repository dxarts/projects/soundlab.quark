// main parameters
IdentityDictionary(know: true)
.putPairs([
	'numSatChans',			6, // the number of satellite speakers
	'numSubChans', 			1, //  the number of sub-woofers, if no subs set to 0
	'outbusNums',           (9..14) ++ [24], //  the actual output channel numbers for each speaker (including subs at the end)
	'beamShape',            'basic', // beam shape of the decoder
	'match',                'amp', // matching happens in the multiband kernel
	'decoderType',          'discrete', // decoder type ('modeMatch', 'panto', 'matrix')
	'discreteType',         'quad', // when discrete, which type of input ('quad', 'hex', 'oct', or 'all'), supplied in LRLR... (when 'discrete' and this is 'all', pass through)
	'encodeType',           true, // when discrete ( not 'all' ) enable transcoding via bformat
	'maxOrder',             2, // highest order this decoder can handle
])
.put('focl',  // remove from Dict if not using Focalization Kernels
	IdentityDictionary.new(know:true)
	.putPairs([
		'psyFoclSize',   1024,
		'alpha',         1.0,
		'foclRadius',    AtkHoa.refRadius,
		'satDim',        2, // 2D for sats
		'match',         'fenergy',
		'sampleRate',    nil, //  set to nil or ommit to use the server sample rate
		'psyFoclKernelPath',     Atk.userExtensionsDir +/+ "foclKernels/" // where do you want to write the kernels (folder will be created if necessary)
	])
)