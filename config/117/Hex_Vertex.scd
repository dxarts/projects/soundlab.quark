// main parameters
IdentityDictionary(know: true)
.putPairs([
	'numSatChans',			6, // the number of satellite speakers
	'numSubChans', 			1, //  the number of sub-woofers, if no subs set to 0
	'outbusNums',           [9, 14, 10, 13, 11, 12] ++ [24], //  the actual output channel numbers for each speaker (including subs at the end)
	'decoderType',          'discrete', // decoder type ('modeMatch', 'panto', 'matrix')
	'discreteType',         'hex_vertex', // when discrete, which type of input ('quad', 'hex', 'oct', or 'all'), supplied in LRLR... (when 'discrete' and this is 'all', pass through)
	'encodeType',           false, // when discrete ( not 'all' ) enable transcoding via bformat
])